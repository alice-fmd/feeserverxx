#include <feeserverxx/fee_lock.hh>
#include <feeserverxx/fee_thread.hh>
#include <pthread.h>
#include <cerrno>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <stdexcept>

//====================================================================
FeeServer::Mutex::Attributes::Attributes(Kind type) 
  : _attr()
{
  pthread_mutexattr_init(&_attr);
  Set(type);
}

//____________________________________________________________________
FeeServer::Mutex::Attributes::~Attributes() 
{
  pthread_mutexattr_destroy(&_attr);
}

//____________________________________________________________________
bool 
FeeServer::Mutex::Attributes::Set(Kind type) 
{
  int kind = 0;
  switch (type) {
  case Recursive: 
#if HAVE_DECL_PTHREAD_MUTEX_RECURSIVE
    kind = PTHREAD_MUTEX_RECURSIVE; 
#elif HAVE_DECL_PTHREAD_MUTEX_RECURSIVE_NP
    kind = PTHREAD_MUTEX_RECURSIVE_NP; 
#else
    std::cerr << "Recursive mutexes not supported" << std::endl;
    kind = PTHREAD_MUTEX_FAST_NP;
#endif
    break;
  case Checking:
#if HAVE_DECL_PTHREAD_MUTEX_ERRORCHECK
    kind = PTHREAD_MUTEX_ERRORCHECK; 
#elif HAVE_DECL_PTHREAD_MUTEX_ERRORCHECK_NP
    kind = PTHREAD_MUTEX_ERRORCHECK_NP; 
#else
    std::cerr << "Error checking mutexes not supported" << std::endl;
    kind = PTHREAD_MUTEX_FAST_NP; 
#endif
    break;
  case Fast:      // Fall through
  default:        kind = PTHREAD_MUTEX_FAST_NP; 	    break;
  }
  return pthread_mutexattr_settype(&_attr, kind) == 0;
}
//____________________________________________________________________
FeeServer::Mutex::Attributes::Kind 
FeeServer::Mutex::Attributes::Type() const 
{
  int kind;
  if (pthread_mutexattr_gettype(&_attr, &kind) != 0) return Fast;
  switch (kind) {
#if HAVE_DECL_PTHREAD_MUTEX_RECURSIVE
  case PTHREAD_MUTEX_RECURSIVE:  return Recursive;
#elif HAVE_DECL_PTHREAD_MUTEX_RECURSIVE_NP
  case PTHREAD_MUTEX_RECURSIVE_NP:  return Recursive;
#endif
#ifdef HAVE_DECL_PTHREAD_MUTEX_ERRORCHECK
  case PTHREAD_MUTEX_ERRORCHECK: return Checking;
#elif HAVE_DECL_PTHREAD_MUTEX_ERRORCHECK_NP
  case PTHREAD_MUTEX_ERRORCHECK_NP: return Checking;
#endif
  default: break;
  }
  return Fast;
}

//====================================================================
FeeServer::Mutex::Mutex(const std::string& name) 
  : _mutex(),
    _name(name)
{
  Attributes attr;
  pthread_mutex_init(&_mutex, &(attr._attr));
  if (!name.empty()) return;
  std::stringstream s;
  s << "0x" << std::hex << std::setfill('0') << std::setw(8) << &_mutex;
  _name = s.str();
}
//____________________________________________________________________
FeeServer::Mutex::Mutex(const std::string& name, const Attributes& attr)
  : _mutex(), 
    _name(name)
{
  pthread_mutex_init(&_mutex, &(attr._attr));
  if (!name.empty()) return;
  std::stringstream s;
  s << "0x" << std::hex << std::setfill('0') << std::setw(8) << &_mutex;
  _name = s.str();
}
//____________________________________________________________________
FeeServer::Mutex::~Mutex() 
{
  pthread_mutex_destroy(&_mutex);
}

//____________________________________________________________________
const char*
FeeServer::Mutex::Return2String(const FeeServer::Mutex::Return& r)
{ 
  switch (r) { 
  case Success:   return "Success";
  case BadMutex:  return "Bad mutex";
  case DeadLock:  return "Would dead-lock";
  case Busy:      return "Is busy"; 
  case Denied:	  return "Permission denied";
  }
  return "";
}

//____________________________________________________________________
FeeServer::Mutex::Return
FeeServer::Mutex::HandleReturn(const char* oper, int ret) const
{ 
  Mutex::Return val = Mutex::Success;
  if (ret == 0) return val;
  std::cerr << oper << " mutex " << _name << " in thread "
	    << ThreadManager::Instance().Current() << " failed: ";
  switch (ret) {
  case EINVAL:  val = BadMutex; std::cerr << "Bad mutex";         break;
  case EDEADLK: val = DeadLock; std::cerr << "Would dead-lock";   break;
  case EBUSY:   val = Busy;	std::cerr << "Is busy";           break;
  case EPERM:   val = Denied;	std::cerr << "Permission denied"; break;
  }
  std::cerr << std::endl;
  return val;
}

//____________________________________________________________________
FeeServer::Mutex::Return
FeeServer::Mutex::Lock()    
{ 
  return HandleReturn("Lock", pthread_mutex_lock(&_mutex)); 
}
//____________________________________________________________________
FeeServer::Mutex::Return
FeeServer::Mutex::TryLock()    
{ 
  return HandleReturn("TryLock", pthread_mutex_trylock(&_mutex));
}
//____________________________________________________________________
FeeServer::Mutex::Return
FeeServer::Mutex::Unlock()    
{ 
  return HandleReturn("Unlock", pthread_mutex_unlock(&_mutex));
}

namespace { 
  std::string 
  mkExceptionString(const char*                     msg, 
		    const FeeServer::Mutex&         m, 
		    const FeeServer::Mutex::Return& r) 
  {
    std::stringstream s;
    s << "Mutex " << m.Name() << ": " << msg << " - " 
      << FeeServer::Mutex::Return2String(r);
    return s.str();
  }
}
//====================================================================
FeeServer::Guard::Guard(Mutex& m) throw (std::exception)
  : _mutex(m)
{
#if 0
  std::cout << "Thread " << ThreadManager::Instance().Current() 
	    << " locking mutex " << _mutex.Name() << std::endl;
#endif
  FeeServer::Mutex::Return ret = _mutex.Lock();
  if (ret != FeeServer::Mutex::Success) {
    std::string msg = mkExceptionString("Failed to lock", _mutex, ret);
    std::cerr << msg << std::endl;
    throw std::runtime_error(msg.c_str());
  }
}

//____________________________________________________________________
FeeServer::Guard::~Guard()
{
#if 0
  std::cout << "Thread " << ThreadManager::Instance().Current() 
	    << " unlocking mutex " << _mutex.Name() << std::endl;
#endif
  FeeServer::Mutex::Return ret = _mutex.Unlock();
  if (ret != FeeServer::Mutex::Success) {
    std::string msg = mkExceptionString("Failed to unlock", _mutex, ret);
    std::cerr << msg << std::endl;
  }
}


//
// EOF
//
