#ifndef FEE_BYTES_HH
#define FEE_BYTES_HH
#include <vector>

namespace FeeServer 
{
  /** type of a byte */
  typedef unsigned char byte;
  /** Type of array of bytes */
  typedef std::vector<unsigned char> byte_array;
  /** Type of a pointer to a byte */
  typedef byte_array::pointer byte_ptr;
}

#endif
//
// EOF
//
