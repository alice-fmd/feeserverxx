#ifndef FEE_SERV_HH
#define FEE_SERV_HH
#include <string>

namespace FeeServer 
{
  /** @defgroup services Services 
      In this group, various strucutres for defining services can be
      found. */
  //__________________________________________________________________
  /** @class ServiceBase fee_serv.hh <feeserverxx/fee_serv.hh> 
      @brief Base class for services 
      @ingroup services */
  class ServiceBase
  {
  public:
    /** Constructor 
	@param name Name of the service */
    ServiceBase(const std::string& name)
      : _name(name), _updated(false), _threshold(0.1)
    {}
    /** Destructor */ 
    virtual ~ServiceBase() { }
    /** Get the name */
    const std::string& Name() const { return _name; }
    /** Return the format to use when declaring the DIM service */
    virtual const char* Format() const { return ""; }
    /** Return the size (in bytes) of the service */ 
    virtual size_t Size() const { return 0; }
    /** Return the address of the data */
    virtual void* Address() { return 0; }
    /** Clear update mark */ 
    virtual void Clear() { _updated = false; }
    /** Whether the service has been updated */ 
    virtual bool Updated() const { return _updated; }
    /** Force an update */
    virtual void ForceUpdate() { _updated = true; }
    /** Set the dead-band */ 
    virtual void SetDeadband(float v) { _threshold = v; }
    /** get the dead-band */ 
    virtual float GetDeadband() const { return _threshold; }
    /** Consistency check */
    virtual bool Consistency() { return true; }
    /** Quality flag */
    virtual int Quality() const { return 0; }
  protected:
    /** Name of service */
    std::string _name;
    /** Whether we've been updated */ 
    bool _updated;
    /** Threshold */
    float _threshold;
  };
}
#endif
//
// EOF
//
