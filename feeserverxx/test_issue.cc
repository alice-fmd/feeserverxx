#include <feeserverxx/fee_thread.hh>
#include <feeserverxx/fee_lock.hh>
#include <feeserverxx/fee_cond.hh>
#include <iostream>
#include <iomanip>
#include <cstdarg>
#include <sstream>
#include <cstdio>
#include <sys/time.h>
#include <unistd.h>

//====================================================================
void Msg(std::ostream& o, const char* prefix, const char* format, va_list ap)
{
  char buf[128], tbuf[128];
  vsnprintf(buf, 128, format, ap);
  struct timeval now;
  gettimeofday(&now, NULL);
  strftime(tbuf, 128, "%T", localtime(&(now.tv_sec)));
  o << prefix << " [" << tbuf << "." 
    << std::setfill('0') << std::setw(3) << now.tv_usec
    << std::setfill(' ') << "] " << buf << std::endl;
}
//____________________________________________________________________
void Info(const char* prefix, const char* format, ...)
{
  va_list ap;
  va_start(ap, format);
  Msg(std::cout, prefix, format, ap);
  va_end(ap);
}
//____________________________________________________________________
void Error(const char* prefix, const char* format, ...)
{
  va_list ap;
  va_start(ap, format);
  Msg(std::cerr, prefix, format, ap);
  va_end(ap);
}

//====================================================================
using namespace FeeServer;

//____________________________________________________________________
struct Issue : public Thread
{
  Issue(Mutex& mutex, 
	Condition& condition,
	unsigned int nStep=4) 
    : _mutex(mutex), 
      _condition(condition), 
      _nstep(nStep)
  {
  }
  void* operator()() 
  {
    Thread::Return ret = Thread::Success;
    Info("==", "Issue thread starting");
    
    // Enable cancels 
    if ((ret = EnableCancel(true)) != Success) {
      Error("==","Unable to configure issue thread for cancellation: %s",
	    Thread::Return2String(ret));
      return 0;
    }

    // Cancel immediately
    if ((ret = AsyncCancel(true)) != Success) {
      Error("==", "Unable to configure issue thread for async cancellation: %s",
	    Thread::Return2String(ret));
      return 0;
    }

    for (unsigned int step = 0; step < _nstep; step++) {
      Info("==", "Issue sleep # %d/%d", step+1, _nstep);
      sleep(1);
    }

    // Defer cancels 
    if ((ret = AsyncCancel(false)) != Success) {
      Error("==","Unable to configure issue thread for async cancellation: %s",
	    Thread::Return2String(ret));
      return 0;
    }

    // Lock wait mutex 
    Mutex::Return mret = Mutex::Success;
    Info("==", "Locking mutex");
    if ((mret = _mutex.Lock()) != Mutex::Success) {
      Error("==","Unable to lock mutex in issue thread: %s",
	    Mutex::Return2String(mret));
      return 0;
    }
      
    // Broad cast that we're done 
    Info("==", "Broadcast");
    _condition.Broadcast();

    // And now unlock the mutex 
    Info("==", "Unlocking mutex");
    if ((mret = _mutex.Unlock()) != Mutex::Success) { 
      Error("==","Unable to release mutex in issue thread: %s", 
	    Mutex::Return2String(mret));
      return 0;
    }

    Info("==", "Issue thread done");
    return 0;
  }
  Mutex&     _mutex;
  Condition& _condition;
  unsigned int          _nstep;
};

//____________________________________________________________________
struct Main : public Thread
{
  Main(unsigned int nCmds, 
       unsigned int delay) // seconds
    : _mutex(), 
      _cmd_mutex(),
      _condition(), 
      _issue(_mutex, _condition, delay), 
      _do_exec(false),
      _timeout(1000 *(delay+1)), 
      _n_cmds(nCmds)
  {
  }
  void Exec()
  {
    Guard g(_cmd_mutex);
    _do_exec = true;
  }
  void* operator()() 
  {
    unsigned int i = 0;
    while (i < _n_cmds) {
      { // Scope for guard
	Guard g(_cmd_mutex);
	if (_do_exec) { 
	  _do_exec = false;
	  Info("--", "Executing issue thread # %d/%d", ++i, _n_cmds);
	  ExecIssue(i);
	}
      }
      usleep(10000);
    }
    return 0;
  }
  void ExecIssue(unsigned int no)
  {
    Info("--", "Locking mutex for %d/%d", no, _n_cmds);
    Guard g(_mutex);
    Info("--", "Mutex locked for %d/%d", no, _n_cmds);

    Info("--", "Starting thread %d/%d", no, _n_cmds);
    Thread::Attributes attr(true);
    Thread::Return     ret = _issue.Run(attr);
    if (ret != Thread::Success) {
      Error("--", "Couldn't make issue thread for  %d/%d: %s", 
	    no, _n_cmds, Thread::Return2String(ret));
      return;
    }

    Info("--", "Wait on condition for %d/%d", no, _n_cmds);
    Condition::Return cret = _condition.Wait(_mutex,_timeout);
    if (cret == Condition::TimeOut) { 
      Error("--", "Timed-out on %d/%d: %s (%dus)", 
	    no, _n_cmds, Condition::Return2String(cret), _timeout);
      return;
    }
    else if (cret != Condition::Success) {
      _issue.Cancel();
      Error("--", "While waiting for issue thread %d/%d: %s", 
	    no, _n_cmds, Condition::Return2String(cret));
      return;
    }	
    Info("--", "End of ExecIssue for %d/%d - mutex unlocked", no, _n_cmds);
    return;
  }
  Mutex        _mutex;
  Mutex        _cmd_mutex;
  Condition    _condition;
  Issue        _issue;
  bool         _do_exec;
  unsigned int _timeout;
  unsigned int _n_cmds;
};

//====================================================================
template <typename T>
void
str2val(const char* s, T& v)
{
  std::stringstream str(s);
  str >> v;
}

//____________________________________________________________________
int
main(int argc, char** argv)
{
  unsigned int delay = 2;
  unsigned int extra = 1;
  unsigned int ncmds = 4;
  bool         usage = false;
  for (int i = 1; i < argc; i++) { 
    if (argv[i][0] == '-') { 
      switch (argv[i][1]) { 
      case 'h': usage = true; break;
      case 'd': str2val(argv[++i], delay); break;
      case 'e': str2val(argv[++i], extra); break;
      case 'n': str2val(argv[++i], ncmds); break;
      default:  usage = true; break;
      }
    }
    else 
      usage = true;
  }

  if (usage) { 
    std::cout << "Usage: " << argv[0] << " [-d DELAY] [-e EXTRA] [-n N]"
	      << std::endl;
    return 0;
  }

  Main m(ncmds, delay+extra);
  m.Run();

  ThreadManager::Instance().List();

  for (unsigned int i = 0; i < ncmds; i++) {
    m.Exec();
    sleep(delay);
  }

  m.Join();

  return 0;
}

      
