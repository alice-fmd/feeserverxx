#ifndef FEE_MSG_HH
#define FEE_MSG_HH
#include <feeserverxx/fee_lock.hh>
#include <feeserverxx/fee_thread.hh>
#include <feeserverxx/fee_packet.hh>
#include <string>
#include <vector>

// Forward decl 
class DimService;

namespace FeeServer 
{
  // Forward decl 
  class MessageWatchdog;

  /** @class MessageChannel fee_msg.hh <feeserverxx/fee_msg.hh> 
      @brief Message channel interface 
      @ingroup channels */
  class MessageChannel
  {
  public:    
    typedef Message::Event Event;
    /** Constructor 
	@param server Server name 
	@param detector Detector name 
	@param mask The log mask */
    MessageChannel(const std::string& server, 
		   const std::string& detector,
		   unsigned long mask=(Message::Info | 
				       Message::Warning | 
				       Message::Error | 
				       Message::AuditFailure | 
				       Message::AuditSuccess |
				       Message::Debug | 
				       Message::Alarm));
    /** Destructor */
    virtual ~MessageChannel();
    
    /** Clear the message structure. */ 
    void Clear();
    /** Send a message 
	@param event The event type 
	@param where Where the event happened. 
	@param format @c printf-like format */ 
    void Send(Event event, const std::string& where, const char* format, ...);
    /** Handle repeated messages */ 
    void HandleRepeats();
    /** Set the log-level */ 
    void SetLogMask(unsigned int mask) { _log_mask = mask | Message::Alarm; }
    /** Get the log mask */
    unsigned int LogMask() const { return _log_mask; }
    /** Check that we want log messages for a type of events */ 
    bool CheckEvent(Event event) const { return _log_mask & event; }
  protected: 
    /** Not implemented */ 
    MessageChannel(const MessageChannel&);
    /** Not implemented */ 
    MessageChannel& operator=(const MessageChannel&);
    /** As Send, except it doesn't lock the mutex */
    void UnlockedSend(Event event, const std::string& where, 
		      const char* format, ...);
    /** Core of the send methods */
    void DoSend(Event event, const std::string& where, const std::string& msg);
    /** Watch dog is a friend */
    friend class MessageWatchdog;

    /** Messagge */
    Message _msg;
    /** The service */
    DimService*  _service;
    /** Mutex */
    Mutex        _mutex;
    /** Last description */
    std::string  _last;
    /** Server name */
    const std::string  _server;
    /** Number of repeats */
    unsigned int _repeats;
    /** Stores the current log level for this FeeServer. In case the
	environmental variable @c FEE_LOG_LEVEL is set, this value
	will be used. */
    unsigned int _log_mask;
    /** Buffer */
    std::vector<unsigned char> _buf;
  };

  //==================================================================
  /** @class MessageWatchdog fee_msg.hh <feeserverxx/fee_msg.hh> 
      @brief Watch dog for of replicated log messages. 

      This watchdog checks, if there have been replicated log messages
      during a time period given by _timeout, which have been hold
      back. The content of these messages is then send including the
      number of how often this has been triggered and hold
      back. Afterwards the counter is set back to zero again and
      backup of the last send log message is cleared.  

      @ingroup processes */
  class MessageWatchdog : public Thread 
  {
  public:
    /** Constructor 
	@param m MEssage channel 
	@param timeout Timeout in milli seconds 
    */
    MessageWatchdog(MessageChannel& m, unsigned int timeout=10000) 
      : Thread("Log watchdog"), 
	_message_channel(m), 
	_timeout(timeout)
    {}
    /** Thread function */
    void* operator()();
    /** Set the time out */ 
    void SetTimeout(unsigned int timeout=10000) { _timeout = timeout; }
    /** Get the time out */
    unsigned int Timeout() const { return _timeout; }
  protected:
    /** Referene to the message channel */
    MessageChannel& _message_channel;
    /** Time out in milliseconds */
    unsigned int _timeout;
  };
}

#endif
//
// EOF
//
