#ifndef FEE_ISSUE_HH
#define FEE_ISSUE_HH
#include <feeserverxx/fee_thread.hh>

namespace FeeServer 
{
  // forward declaration 
  class Main;
  class Command;
  class Acknowledge;

  //__________________________________________________________________
  /** @class IssueThread fee_issue.hh <feeserverxx/fee_issue.hh> 
      @brief Thread that forwards command requests to the control
      engine 
      @ingroup processes */
  class IssueThread : public Thread 
  {
  public:
    /** Constructor */
    IssueThread(Main& main) 
      : Thread("Issue"), _main(main), _command(0), _acknowledge(0) {}
    /** Set the command information */ 
    void SetCommand(Command& c)         { _command = &c; }
    /** Set the acknowledge information */ 
    void SetAcknowledge(Acknowledge& a) { _acknowledge = &a; }
    /** Do the thread thing */
    void* operator()();
  protected: 
    /** Not implemented */
    IssueThread(const IssueThread&);
    /** Not implemented */ 
    IssueThread& operator=(const IssueThread&);
    
    /** Reference to server. */
    Main& _main;
    /** Command information */ 
    Command* _command;
    /** Acknowledge information */ 
    Acknowledge* _acknowledge;
  };
}

#endif
//
// EOF
//

