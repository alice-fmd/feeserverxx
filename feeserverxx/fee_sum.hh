#ifndef FEE_SUM_HH
#define FEE_SUM_HH
#include <feeserverxx/fee_bytes.hh>

namespace FeeServer 
{
  /** Calculate the check sum of passed buffer.
      @param data Buffer calculate check sum for
      @param size Size if buffer 
      @return the check sum 
      @ingroup packet
  */
  inline unsigned int 
  CheckSum(const byte_ptr data, size_t size) 
  {
    if (!data) return 0;
    // Define for the Adler base; largest prime number smaller than
    // 2^16 (65536). 
    const unsigned long adler_base = 65521;
    unsigned long adler            = 1L;
    unsigned long part1            = adler & 0xffff;
    unsigned long part2            = (adler >> 16) & 0xffff;
    for (size_t n = 0; n < size; n++) {
      part1 = (part1 + data[n]) % adler_base;
      part2 = (part2 + part1)   % adler_base;
    }
    return (unsigned int)((part2 << 16) + part1);
  }
}
#endif
//
// EOF
//

