#include <feeserverxx/fee_thread.hh>
#include <feeserverxx/fee_lock.hh>
#include <iostream>
#include <unistd.h>

FeeServer::Mutex lock;

/** A test thread 
    @ingroup tests */
class Thread : public FeeServer::Thread 
{
public:
  Thread(char id, size_t n) : _id(id), _n(n) {}
  void* operator()() 
  {
    std::cout << "Thread " << _id << " started" << std::endl;
    for (size_t i = 0; i < _n; i++) { 
      std::cout << _id << "-" << i << " " << std::flush;
      usleep(10);
    }
    std::cout << "Thread " << _id << " locking" << std::endl;
    FeeServer::Guard g(lock);
    for (size_t i = 0; i < _n; i++) { 
      std::cout << _id << "-" << i << " " << std::flush;
      usleep(10);
    }
    std::cout << "Thread " << _id << " finished" << std::endl;
    return 0;
  }
  char _id;
  size_t _n;
};


int main()
{
  Thread t1('a', 10);
  Thread t2('b', 15);

  t1.Run();
  t2.Run();

  t2.Join();
  t1.Join();

  return 0;
}

