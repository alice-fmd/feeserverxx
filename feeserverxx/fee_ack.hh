#ifndef FEE_ACK_HH
#define FEE_ACK_HH
#include <string>
#include <feeserverxx/fee_bytes.hh>

// Forward declaration 
class DimService;

namespace FeeServer 
{
  // Forward decl 
  class Acknowledge;

  /** @defgroup channels Core Feeserver channels.
      @ingroup core
      These classes defines the interface to the surrounding world */
      
  /** @class AcknowledgeChannel fee_ack.hh <feeserverxx/fee_ack.hh> 
      @brief The acknowledge channel 
      @ingroup channels */
  class AcknowledgeChannel 
  {
  public:
    /** constructor 
	@param server Server name */
    AcknowledgeChannel(const std::string& server);
    /** Destructor */
    virtual ~AcknowledgeChannel();

    /** Send the acknowledge back 
	@param ack The acknowledge structure */ 
    void Send(Acknowledge& ack);
  protected:
    /** Not implemented */ 
    AcknowledgeChannel(const AcknowledgeChannel&);
    /** Not implemented */ 
    AcknowledgeChannel& operator=(const AcknowledgeChannel&);
    /** The DIM service */
    DimService* _service;
    /** Name of the server */ 
    std::string _server;
    /** buffer of acknowledge data */ 
    byte_array _buffer;
  };
}
#endif
//
// EOF
//
