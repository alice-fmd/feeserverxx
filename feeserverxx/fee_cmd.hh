#ifndef FEE_CMD_HH
#define FEE_CMD_HH
#include <string>
#include <feeserverxx/fee_packet.hh>

// Forward decl 
class DimCommand;

namespace FeeServer 
{
  // Forward declaration 
  class Main;
  class CommandHandler;

  /** @class CommandChannel fee_cmd.hh <feeserverxx/fee_cmd.hh>
      @brief Command channel 
      @ingroup channels */
  class CommandChannel 
  {
  public:
    /** Constructor */ 
    CommandChannel(const std::string& server, Main& main);
    /** Destructor */
    virtual ~CommandChannel();
  protected: 
    /** Not implemented */ 
    CommandChannel(const CommandChannel&);
    /** Not implemented */ 
    CommandChannel& operator=(const CommandChannel&);
    /** Handle the commands. In this class we simply turn the passed
	data into a proper format, and pass that on to the main
	object.  This object will in turn do the actual processing,
	possibly sending the request on to the control engine. 
	@param data Command channel data 
	@param size Size of @a data */
    void Handle(const byte_ptr data, size_t size);
    /** Back reference to the FeeServer */
    Main& _main;
    /** Current command */ 
    Command _cmd;
    /** Command handler is a friend */
    friend class CommandHandler;
    /** Command handler */ 
    CommandHandler* _handler;
  };
}

#endif
//
// EOF
//

