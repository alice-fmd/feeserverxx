#ifndef FEE_SCMD_HH
#define FEE_SCMD_HH
#include <cctype>
#include <feeserverxx/fee_packet.hh>

namespace FeeServer 
{
  // Forward decl 
  class Main;

  /** @defgroup scmd Server commands 
      @ingroup core 
      This group contains verious server commands */
  //__________________________________________________________________
  /** @class ServerCommand fee_scmd.hh <feeserverxx/fee_scmd.hh>
      @brief base class for server commands 
      @ingroup scmd
  */
  class ServerCommand 
  {
  public:
    /** Constructor 
	@param m Reference to server
	@param mask Mask of bits to look for */ 
    ServerCommand(Main& m, unsigned short mask);
    /** Destructor */ 
    virtual ~ServerCommand() {}
    /** Check if we can handle a request 
	@param flag The request flag
	@return true if we should handle this request */ 
    virtual bool CanHandle(unsigned short flag) const { return flag & _mask; }
    /** Handle the command. 
	@param idata Input data 
	@param isize Size of @a idata 
	@param odata Reference to pointer.  On output it should
	contain the output data. 
	@param osize On output, the size of @a odata 
	@return Error code or return value */
    virtual short Handle(const byte_array& idata, 
			 const size_t                      isize, 
			 byte_array&       odata, 
			 size_t&                           osize) = 0;
    /** Get the mask */ 
    unsigned short Mask() const { return _mask; }
  protected:
    /** Flag */ 
    const unsigned short _mask;
    /** Reference to server object */
    Main& _main;
  };
  
  //__________________________________________________________________
  /** @class UpdateServer fee_scmd.hh <feeserverxx/fee_scmd.hh>
      @brief Update the feeserver executable
      The expected payload is 
      - the bytes of a valid feeserver program
      @ingroup scmd
  */
  struct UpdateServer : public ServerCommand 
  {
    /** constructor 
	@param m Reference to server */
    UpdateServer(Main& m) : ServerCommand(m, Command::UpdateServer) {}
    /** Handle the command. 
	@param idata Input data 
	@param isize Size of @a idata 
	@param odata Reference to pointer.  On output it should
	contain the output data. 
	@param osize On output, the size of @a odata 
	@return Error code or return value */
    short Handle(const byte_array& idata, 
		 const size_t                      isize, 
		 byte_array&       odata, 
		 size_t&                           osize);
  };
  //__________________________________________________________________
  /** @class RestartServer fee_scmd.hh <feeserverxx/fee_scmd.hh>
      @brief Restart the feeserver
      The expected payload is none
      @ingroup scmd
  */
  struct RestartServer : public ServerCommand 
  {
    /** constructor 
	@param m Reference to server */
    RestartServer(Main& m) : ServerCommand(m, Command::RestartServer) {}
    /** Handle the command. 
	@param idata Input data 
	@param isize Size of @a idata 
	@param odata Reference to pointer.  On output it should
	contain the output data. 
	@param osize On output, the size of @a odata 
	@return Error code or return value */
    short Handle(const byte_array& idata, 
		 const size_t                      isize, 
		 byte_array&       odata, 
		 size_t&                           osize);
  };
  //__________________________________________________________________
  /** @class RebootMachine fee_scmd.hh <feeserverxx/fee_scmd.hh>
      @brief Reboot the Linux machine
      The expected payload is none
      @ingroup scmd
  */
  struct RebootMachine : public ServerCommand 
  {
    /** constructor 
	@param m Reference to server */
    RebootMachine(Main& m) : ServerCommand(m, Command::RebootMachine) {}
    /** Handle the command. 
	@param idata Input data 
	@param isize Size of @a idata 
	@param odata Reference to pointer.  On output it should
	contain the output data. 
	@param osize On output, the size of @a odata 
	@return Error code or return value */
    short Handle(const byte_array& idata, 
		 const size_t                      isize, 
		 byte_array&       odata, 
		 size_t&                           osize);
  };
  //__________________________________________________________________
  /** @class ShutdownMachine fee_scmd.hh <feeserverxx/fee_scmd.hh>
      @brief Power off the Linux machine
      The expected payload is none 
      @ingroup scmd
  */
  struct ShutdownMachine : public ServerCommand 
  {
    /** constructor 
	@param m Reference to server */
    ShutdownMachine(Main& m) : ServerCommand(m, Command::ShutdownMachine) {}
    /** Handle the command. 
	@param idata Input data 
	@param isize Size of @a idata 
	@param odata Reference to pointer.  On output it should
	contain the output data. 
	@param osize On output, the size of @a odata 
	@return Error code or return value */
    short Handle(const byte_array& idata, 
		 const size_t                      isize, 
		 byte_array&       odata, 
		 size_t&                           osize);
  };
  //__________________________________________________________________
  /** @class ExitServer fee_scmd.hh <feeserverxx/fee_scmd.hh>
      @brief Stop the server
      The expected payload is none
      @ingroup scmd
  */
  struct ExitServer : public ServerCommand 
  {
    /** constructor 
	@param m Reference to server */
    ExitServer(Main& m) : ServerCommand(m, Command::ExitServer) {}
    /** Handle the command. 
	@param idata Input data 
	@param isize Size of @a idata 
	@param odata Reference to pointer.  On output it should
	contain the output data. 
	@param osize On output, the size of @a odata 
	@return Error code or return value */
    short Handle(const byte_array& idata, 
		 const size_t                      isize, 
		 byte_array&       odata, 
		 size_t&                           osize);
  };
  //__________________________________________________________________
  /** @class SetDeadband fee_scmd.hh <feeserverxx/fee_scmd.hh>
      @brief  sets the deadband for a specified item(s).
      The expected payload is 
      - threshold (float) New dead-band value
      - name (string) Name of service (sans server prefix) 
        to set dead-band for. This may be a a wild card of the form @e
	*_name in which case all services that end in @e _name will
	get the new dead-band. 
      @ingroup scmd
  */
  struct SetDeadband : public ServerCommand 
  {
    /** constructor 
	@param m Reference to server */
    SetDeadband(Main& m) : ServerCommand(m, Command::SetDeadband) {}
    /** Handle the command. 
	@param idata Input data 
	@param isize Size of @a idata 
	@param odata Reference to pointer.  On output it should
	contain the output data. 
	@param osize On output, the size of @a odata 
	@return Error code or return value */
    short Handle(const byte_array& idata, 
		 const size_t                      isize, 
		 byte_array&       odata, 
		 size_t&                           osize);
  };
  //__________________________________________________________________
  /** @class GetDeadband fee_scmd.hh <feeserverxx/fee_scmd.hh>
      @brief Get the deadband for a specified item.
      The expected payload is 
      - name (string) Name of service (sans server prefix) 
        to get dead-band for
      @ingroup scmd
  */
  struct GetDeadband : public ServerCommand 
  {
    /** constructor 
	@param m Reference to server */
    GetDeadband(Main& m) : ServerCommand(m, Command::GetDeadband) {}
    /** Handle the command. 
	@param idata Input data 
	@param isize Size of @a idata 
	@param odata Reference to pointer.  On output it should
	contain the output data. 
	@param osize On output, the size of @a odata 
	@return Error code or return value */
    short Handle(const byte_array& idata, 
		 const size_t                      isize, 
		 byte_array&       odata, 
		 size_t&                           osize);
  };
  //__________________________________________________________________
  /** @class SetIssueTimeout fee_scmd.hh <feeserverxx/fee_scmd.hh>
      @brief Set the time out for the issue command.
      The expected payload is 
      - timeout (long) The new timeout in milliseconds 
      @ingroup scmd
  */
  struct SetIssueTimeout : public ServerCommand 
  {
    /** constructor 
	@param m Reference to server */
    SetIssueTimeout(Main& m) : ServerCommand(m, Command::SetIssueTimeout) {}
    /** Handle the command. 
	@param idata Input data 
	@param isize Size of @a idata 
	@param odata Reference to pointer.  On output it should
	contain the output data. 
	@param osize On output, the size of @a odata 
	@return Error code or return value */
    short Handle(const byte_array& idata, 
		 const size_t                      isize, 
		 byte_array&       odata, 
		 size_t&                           osize);
  };
  //__________________________________________________________________
  /** @class GetIssueTimeout fee_scmd.hh <feeserverxx/fee_scmd.hh>
      @brief Get the time out for the issue command.
      The expected payload is none
      @ingroup scmd
  */
  struct GetIssueTimeout : public ServerCommand 
  {
    /** constructor 
	@param m Reference to server */
    GetIssueTimeout(Main& m) : ServerCommand(m, Command::GetIssueTimeout) {}
    /** Handle the command. 
	@param idata Input data 
	@param isize Size of @a idata 
	@param odata Reference to pointer.  On output it should
	contain the output data. 
	@param osize On output, the size of @a odata 
	@return Error code or return value */
    short Handle(const byte_array& idata, 
		 const size_t                      isize, 
		 byte_array&       odata, 
		 size_t&                           osize);
  };
  //__________________________________________________________________
  /** @class SetUpdateRate fee_scmd.hh <feeserverxx/fee_scmd.hh>
      @brief Set the monitor (service) update rate
      The expected payload is 
      - rate (short) New update rate 
      @ingroup scmd
  */
  struct SetUpdateRate : public ServerCommand 
  {
    /** constructor 
	@param m Reference to server */
    SetUpdateRate(Main& m) : ServerCommand(m, Command::SetUpdateRate) {}
    /** Handle the command. 
	@param idata Input data 
	@param isize Size of @a idata 
	@param odata Reference to pointer.  On output it should
	contain the output data. 
	@param osize On output, the size of @a odata 
	@return Error code or return value */
    short Handle(const byte_array& idata, 
		 const size_t                      isize, 
		 byte_array&       odata, 
		 size_t&                           osize);
  };
  //__________________________________________________________________
  /** @class GetUpdateRate fee_scmd.hh <feeserverxx/fee_scmd.hh>
      @brief Get the monitor (service) update rate
      The expected payload is none
      @ingroup scmd
  */
  struct GetUpdateRate : public ServerCommand 
  {
    /** constructor 
	@param m Reference to server */
    GetUpdateRate(Main& m) : ServerCommand(m, Command::GetUpdateRate) {}
    /** Handle the command. 
	@param idata Input data 
	@param isize Size of @a idata 
	@param odata Reference to pointer.  On output it should
	contain the output data. 
	@param osize On output, the size of @a odata 
	@return Error code or return value */
    short Handle(const byte_array& idata, 
		 const size_t                      isize, 
		 byte_array&       odata, 
		 size_t&                           osize);
  };
  //__________________________________________________________________
  /** @class SetLogLevel fee_scmd.hh <feeserverxx/fee_scmd.hh>
      @brief Set the log mask
      The expected payload is 
      - mask (int) The new log mask 
      @ingroup scmd
  */
  struct SetLogLevel : public ServerCommand 
  {
    /** constructor 
	@param m Reference to server */
    SetLogLevel(Main& m) : ServerCommand(m, Command::SetLogLevel) {}
    /** Handle the command. 
	@param idata Input data 
	@param isize Size of @a idata 
	@param odata Reference to pointer.  On output it should
	contain the output data. 
	@param osize On output, the size of @a odata 
	@return Error code or return value */
    short Handle(const byte_array& idata, 
		 const size_t                      isize, 
		 byte_array&       odata, 
		 size_t&                           osize);
  };
  //__________________________________________________________________
  /** @class GetLogLevel fee_scmd.hh <feeserverxx/fee_scmd.hh>
      @brief Get the log mask
      The expected payload is none
      @ingroup scmd
  */
  struct GetLogLevel : public ServerCommand 
  {
    /** constructor 
	@param m Reference to server */
    GetLogLevel(Main& m) : ServerCommand(m, Command::GetLogLevel) {}
    /** Handle the command. 
	@param idata Input data 
	@param isize Size of @a idata 
	@param odata Reference to pointer.  On output it should
	contain the output data. 
	@param osize On output, the size of @a odata 
	@return Error code or return value */
    short Handle(const byte_array& idata, 
		 const size_t                      isize, 
		 byte_array&       odata, 
		 size_t&                           osize);
  };
  //__________________________________________________________________
  /** @class ShowServices fee_scmd.hh <feeserverxx/fee_scmd.hh>
      @brief Show the services monitored
      The expected payload is none
      @ingroup scmd
  */
  struct ShowServices : public ServerCommand 
  {
    /** constructor 
	@param m Reference to server */
    ShowServices(Main& m) : ServerCommand(m, Command::ShowServices) {}
    /** Handle the command. 
	@param idata Input data 
	@param isize Size of @a idata 
	@param odata Reference to pointer.  On output it should
	contain the output data. 
	@param osize On output, the size of @a odata 
	@return Error code or return value */
    short Handle(const byte_array& idata, 
		 const size_t                      isize, 
		 byte_array&       odata, 
		 size_t&                           osize);
  };
  //__________________________________________________________________
  /** @class SuspendMonitoring fee_scmd.hh <feeserverxx/fee_scmd.hh>
      @brief Suspends monitoring
      @ingroup scmd
  */
#if 0
  struct SuspendMonitoring : public ServerCommand 
  {
    /** constructor 
	@param m Reference to server */
    SuspendMonitoring(Main& m) : ServerCommand(m, Command::SuspendMonitoring) {}
    /** Handle the command. 
	@param idata Input data 
	@param isize Size of @a idata 
	@param odata Reference to pointer.  On output it should
	contain the output data. 
	@param osize On output, the size of @a odata 
	@return Error code or return value */
    short Handle(const byte_array& idata, 
		 const size_t                      isize, 
		 byte_array&       odata, 
		 size_t&                           osize);
  };
  //__________________________________________________________________
  /** @class ResumeMonitoring fee_scmd.hh <feeserverxx/fee_scmd.hh>
      @brief Resumes monitoring
      @ingroup scmd
  */
  struct ResumeMonitoring : public ServerCommand 
  {
    /** constructor 
	@param m Reference to server */
    ResumeMonitoring(Main& m) : ServerCommand(m, Command::ResumeMonitoring) {}
    /** Handle the command. 
	@param idata Input data 
	@param isize Size of @a idata 
	@param odata Reference to pointer.  On output it should
	contain the output data. 
	@param osize On output, the size of @a odata 
	@return Error code or return value */
    short Handle(const byte_array& idata, 
		 const size_t                      isize, 
		 byte_array&       odata, 
		 size_t&                           osize);
  };
#endif
}

#endif
//
// EOF
//
