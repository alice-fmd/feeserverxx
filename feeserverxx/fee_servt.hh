#ifndef FEE_SERVT_HH
#define FEE_SERVT_HH
#include <feeserverxx/fee_serv.hh>
#include <feeserverxx/fee_sum.hh>
#include <cmath>
#include <cstdlib>
#include <iostream>

namespace FeeServer 
{
  //__________________________________________________________________
  /** @class ServiceT fee_servt.hh <feeserverxx/fee_servt.hh> 
      @brief Class template for simple type services
      @param T Type of the value to serve 
      @param Trait Traits of type T (@see FloatTrait @see IntTrait)
      @ingroup services 
  */
  template <typename T, typename Trait>
  class ServiceT : public ServiceBase
  {
  public:
    /** Constructor */
    ServiceT(const std::string& name, const T& def=Trait::Default()) 
      : ServiceBase(name), 
	_value(def), 
	_old(def),
	_check_sum(0),
	_old_check_sum(0),
	_quality(0)
    {
      _check_sum     = CheckSum(_value);
      _old_check_sum = CheckSum(_old);
    }
    /** Destructor */ 
    virtual ~ServiceT() {}
    /** Get format string used for DIM service declaration */
    const char* Format() const { return Trait::Format(); }
    /** Size of the data */
    size_t Size() const { return Trait::Size(); }
    /** Get the address of the data served */
    void* Address() { return reinterpret_cast<void*>(&_value); }
    /** Clear updated mark, and assign old to current value */
    void Clear() 
    {
      Trait::Copy(_value, _old);
      _old_check_sum = CheckSum(_old);
      ServiceBase::Clear();
    }
    /** Assign from value  */
    ServiceT& operator=(const T& v) 
    {
      Trait::Copy(v, _value);
      _check_sum = CheckSum(_value);
      if (!Trait::Compare(_value, _old, _threshold)) _updated = true;
      return *this;
    }
    /** Check that data is consistent */
    bool Consistency() 
    {
      // If old and new are equal, everything is good.
      if (Trait::Equal(_value, _old)) return true; 
      
      // Values are not identical - check more. 
      // If the check sum of the value is OK, update old
      if (_check_sum == CheckSum(_value)) {
	Trait::Copy(_value, _old);
	return true;
      }
      
      // Check sum is currupted, or location is broken
      // If check sum of stored value is ok, then update check sum
      if (_check_sum == CheckSum(_old)) {
	Trait::Copy(_old, _value);
	return true;
      }

      // backup value is broken or check sum is bad 
      // If check sums are equal, then both values are broken and
      // there's nothing we can do about it 
      if (_check_sum == _old_check_sum) return false;

      // Check sum is probably bad. 
      // If old check sum matches value, we can restore the check sum 
      if (_old_check_sum == CheckSum(_value)) {
	_check_sum = _old_check_sum;
	return true;
      }

      // It seems that value or back up check sum is broken 
      // If backup checksum and backup value is good, restore from
      // that. 
      if (_old_check_sum == CheckSum(_old)) {
	Trait::Copy(_old, _value);
	_check_sum = _old_check_sum;
      }

      // Everything is "banana" - give up. 
      return false;
    }
    /** Get reference to the value */ 
    const T& Value() const { return _value; }
    /** Get the quality */
    int Quality() const { return _quality; }
    /** Set the quality flag */
    void SetQuality(int q) { _quality = q; }
  protected:
    /** Calculate the check sum of @a v */
    unsigned int CheckSum(const T& v) const 
    { 
      unsigned char* data = (unsigned char*)(&v);
      return FeeServer::CheckSum(data, Size());
    }
    /** Current value */
    T _value;
    /** Old value */
    T _old;
    /** Check sum of value */
    unsigned int _check_sum;
    /** Check sum of old value */
    unsigned int _old_check_sum;
    /** Quality flag */
    int _quality;
  };
  
  //__________________________________________________________________
  /** @class FloatTrait fee_servt.hh <feeserverxx/fee_servt.hh> 
      @brief Traits for floating point values 
      @ingroup services
  */
  struct FloatTrait 
  { 
  public:
    /** copy value of @a src to @a dest */
    static void Copy(const float& src, float& dest) { dest = src; }
    /** Check if difference between @a v1 and @a v2 is not bigger than
	@a threshold */
    static bool Compare(const float& v1, const float& v2,
			const float& threshold) 
    {
      return (fabs(v1-v2) < threshold);
    }
    /** Check if @a v1 and @a v2 are equal */
    static bool Equal(const float& v1, const float& v2) 
    {
      return v1 == v2;
    }
    /** Return the format string */
    static const char* Format() { return "F"; }
    /** Get the size (in bytes) of a value */
    static size_t Size() { return sizeof(float); }
    /** Get the default value */
    static float Default() { return float(); }
    static std::string AsString();
  };

  //__________________________________________________________________
  /** @class IntTrait fee_servt.hh <feeserverxx/fee_servt.hh> 
      @brief Traits for integer values
      @ingroup services
  */
  struct IntTrait 
  { 
  public:
    /** copy value of @a src to @a dest */
    static void Copy(const int& src, int& dest) { dest = src; }
    /** Check if difference between @a v1 and @a v2 is not bigger than
	@a threshold */
    static bool Compare(const int& v1, const int& v2,
			const float& threshold) 
    {
      return (abs(v1-v2) < threshold);
    }
    /** Check if @a v1 and @a v2 are equal */
    static bool Equal(const int& v1, const int& v2) 
    {
      return v1 == v2;
    }
    /** Return the format string */
    static const char* Format() { return "I"; }
    /** Get the size (in bytes) of a value */
    static size_t Size() { return sizeof(int); }
    /** Get the default value */
    static int Default() { return -1; }
    static std::string AsString();
  };



  //__________________________________________________________________
  /** Service specialised for integer values 
      @ingroup services 
  */ 
  typedef ServiceT<int,IntTrait> IntService;

  //__________________________________________________________________
  /** Service specialised for floating point values 
      @ingroup services 
  */
  typedef ServiceT<float,FloatTrait> FloatService;
}

#endif
//
// EOF
//
