#ifndef FEE_CLIENT_HH
#define FEE_CLIENT_HH
#include <vector>
#include <string>
#include <feeserverxx/fee_packet.hh>

namespace FeeServer
{ 
  /** @defgroup client Client code */
  // Forward declaration of hidden DIM info handler
  class InfoHandler;

  /** @class Client fee_client.hh <feeserverxx/fee_client.hh>
      @brief An example client 
      @ingroup client */
  class Client 
  {
  public:
    /** Constructor */
    Client(const std::string& server, 
	   const std::string& dns, 
	   unsigned int tries=20, 
	   unsigned int timeout=2000);
    /** Destructor */
    virtual ~Client();

    /** Send a command to the server */ 
    bool SendCommand(unsigned short flags, std::vector<unsigned char>& data);
    /** Function called when messages arrive 
	@param m Messsage structure */
    virtual void HandleMessage(Message& m);
    /** Called on no-link to message channel */ 
    virtual void NolinkMessage() {}
    /** Called on no-link to acknowledge channel */
    virtual void NolinkAcknowledge() {}
    /** Acknowledge error seen 
	@param error Error code */
    virtual void AcknowledgeError(short error);
    /** Bad check sum in acknowledge 
	@param expected Expected check sum 
	@param got      The actual check sum */
    virtual void ChecksumFailed(unsigned int expected, unsigned int got);
    /** Got unexpected reply 
	@param expected Expected reply 
	@param got      The actual reply */
    virtual void UnexpectedReply(unsigned int expected, unsigned int got);
    /** Got an unknown service update 
	@param name Name of service updated */
    virtual void UnknownService(const std::string& name);
    /** Called when the acknowledge times out */
    virtual void Timeout() {}
  protected:
    /** Not implemented */
    Client(const Client&);
    /** Not implemented */
    Client& operator=(const Client&);
    /** Make a request id */ 
    virtual unsigned int MakeId();
    /** Information handler */
    // void infoHandler();
    /** Handle the acknowledge */ 
    void DoHandleAcknowledge();
    /** Hidden info handler is a friend */
    friend class InfoHandler;
    /** Pointer to hidden info handler */
    InfoHandler* _handler;
    /** server name */ 
    std::string _server;
    /** Name of command service */ 
    std::string _cmd_name;
    /** Message structure */
    Message _msg;
    /** Acknowledge structure */ 
    Acknowledge _ack;
    /** Command structure */
    Command _cmd;
    /** Buffer */ 
    std::vector<unsigned char> _buffer;
    /** Current number of valid bytes in buffer */ 
    size_t _size;
    /** Tries */ 
    unsigned int _tries;
    /** Timeout otu */ 
    unsigned int _timeout;
    /** Got the reply */
    bool _reply;
    /** Id we're waiting for */
    unsigned int _id;
  };
  inline void Client::HandleMessage(Message&) {}
  inline void Client::AcknowledgeError(short) {}
  inline void Client::ChecksumFailed(unsigned int, unsigned int) {}
  inline void Client::UnexpectedReply(unsigned int, unsigned int) {}
  inline void Client::UnknownService(const std::string&) {}
  
}
#endif
//
// EOF
// 
