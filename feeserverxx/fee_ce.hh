#ifndef FEE_CE_HH
#define FEE_CE_HH
#include <feeserverxx/fee_thread.hh>
#include <feeserverxx/fee_lock.hh>
#include <feeserverxx/fee_cond.hh>
#include <feeserverxx/fee_bytes.hh>

namespace FeeServer 
{
  // Forward decl 
  class Main;
  /** @defgroup ctrl_engine Control engine interface */
  /** @class ControlEngine fee_ce.hh <feeserverxx/fee_ce.hh>
      @brief Control engine abstract interface 
      @ingroup ctrl_engine */
  class ControlEngine : public Thread
  {
  public:
    /** Constructor */
    ControlEngine(Main& main);
    /** Destructor */
    virtual ~ControlEngine() {}

    /** Main function.  
	When the CE is ready, it should call the member function Ready
	from this function */
    virtual void Start() = 0;
    /** Clean up handler */ 
    virtual void CleanUp() = 0;
    /** Handle a command 
	@param idata Input data - encodes the command. 
	@param isize Size of @a idata 
	@param odata On return, contains the output data 
	@param osize On return, contains the size of @a odata */
    virtual short Issue(const byte_array&  idata, 
			size_t             isize, 
			byte_array&        odata, 
			size_t&            osize) = 0;
    /** Notification of update rate */ 
    virtual void UpdateRate(unsigned int) {}
  protected: 
    /** Thread function */ 
    void* operator()();
    /** Signal that we're ready */ 
    void Ready();
    /** Main is a friend */
    friend class Main;
    /** Reference to server main */
    Main& _main;
    /** Mutex to lock */ 
    Mutex _mutex;
    /** Condition variable */ 
    Condition _condition;
    /** Whether we're ready */ 
    bool _is_ready;
    /** Whether we're ready */ 
    bool _can_continue;
  };
}

#endif

    
