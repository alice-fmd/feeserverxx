#include <fee_main.hh>
#include <fee_scmd.hh>
#include <fee_servt.hh>
#include <fee_ce.hh>
#include <fee_item.hh>
#include <fee_dbg.hh>
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <cstring>
#include <sstream>

namespace Examples 
{
  /** A test control engine 
      @ingroup tests */
  class TestControlEngine : public FeeServer::ControlEngine 
  {
  public:
    /** Constructor 
	@param m Reference to FeeServer */
    TestControlEngine(FeeServer::Main& m) 
      : FeeServer::ControlEngine(m), 
	_int_service("INT"), 
	_float_service("FLOAT")
    {
      m.Publish(_int_service);
      m.Publish(_float_service);
    }
    /** Start the thread */
    void Start() 
    {
      usleep(5);
      Ready();

      sleep(1);
      while (true) {
	TestCancel();
	int   ival = int(10 * float(rand())/RAND_MAX);
	float fval = int(10 * float(rand())/RAND_MAX) / 10.;
	_int_service   = ival;
	_float_service = fval;
	usleep(100000);
      }
    } //End-Start
    /** Handle commands send to the control engine 
	@param idata Input data 
	@param isize # of valid bytes in @a idata 
	@param odata On output, the result. 
	@param osize On output, the # of valid bytes in @a odata 
	@return Status code */
    short Issue(const std::vector<unsigned char>&  idata, size_t  /*isize*/,
		std::vector<unsigned char>& odata, size_t& osize) 
    {
      switch (idata[0]) {
      case 1: 
	osize = _int_service.Size();
	if (odata.size() < osize) odata.resize(osize);
	memcpy(&(odata[0]), &(_int_service.Value()), osize);
	break;
      case 2: 
	osize = _float_service.Size();
	if (odata.size() < osize) odata.resize(osize);
	memcpy(&(odata[0]), &(_float_service.Value()), osize);
	break;
      case 3:
	osize = 0;
	std::cout << "Services to monitor: " << std::endl;
	for (FeeServer::MonitorThread::ItemMap::iterator i = 
	       _main.MonThread().Items().begin(); 
	     i != _main.MonThread().Items().end(); i++) 
	  std::cout << "\t" << i->first << "\t" << i->second->Name() 
		    << std::endl;
	break;
      default:
	osize = 0;
      }
      return 0;
    } //End-Issue
    /** Clean-up member function */
    void CleanUp() {}
    // An integer service 
    FeeServer::IntService _int_service;
    // A floating point service 
    FeeServer::FloatService _float_service;
  };
} //End-Example

template <typename T>
T& 
str2val(const char* a, T& v) 
{
  std::stringstream s(a);
  s >> v;
  return v;
}
template <>
unsigned int& 
str2val<unsigned int>(const char* a, unsigned int& v) 
{
  std::stringstream s(a);
  if (a[0] == '0') { 
    if (a[1] == 'x' || a[1] == 'X') s << std::hex;
    else s << std::oct;
  }
  s >> v;
  return v;
}


int 
main(int argc, char** argv) 
{
  std::string  name     = "dummy";
  std::string  detector = "my";
  std::string  dns      = "localhost";
  bool         debug    = false;
  unsigned int trace    = 0;
  for (int i = 1; i < argc; i++) {
    std::cout << "Proccesing option " << i << "/" << argc << ": " 
	      << argv[i] << std::endl;
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'h': 
	std::cout << "Usage: " << argv[0] << " OPTIONS\n\n" 
		  << "Options:\n" 
		  << "\t-h\t\tThis help\n" 
		  << "\t-n NAME\t\tSet name of server\n" 
		  << "\t-d HOST\t\tSet the DIM DNS host name\n"
		  << "\t-D DETECTOR\tSet detector name\n" 
		  << "\t-u\t\tEnable debug messages\n"
		  << "\t-t LEVEL\t\tSet the trace level\n"
		  << std::endl;
	return 0;
      case 'n': name     = argv[++i];                 break;
      case 'd': dns      = argv[++i];                 break;
      case 'D': detector = argv[++i];                 break;
      case 'u': debug    = true;                      break;
      case 't': trace    = str2val(argv[++i], trace); break;
      default: 
	std::cerr << argv[0] << ": unknown option " << argv[i] << std::endl;
	return 1;
      }
    }
  }
  DTRACE(trace);
  
  std::cout << "Creating feeserver" << std::endl;
  FeeServer::Main m(name, detector, dns);
  if (debug) {
    unsigned int mask = m.GetLogLevel();
    std::cout << "Log mask: 0x" << std::hex << mask << " -> ";
    mask |= FeeServer::Message::Debug;
    std::cout << "0x" << mask << std::dec << std::endl;
    m.SetLogLevel(mask);
  }

  // m.AddCommand(new UpdateServer(m));
  m.AddCommand(new FeeServer::RestartServer(m));
  // m.AddCommand(new FeeServer::RebootMachine(m));
  // m.AddCommand(new FeeServer::ShutdownMachine(m));
  m.AddCommand(new FeeServer::ExitServer(m));
  m.AddCommand(new FeeServer::SetDeadband(m));
  m.AddCommand(new FeeServer::GetDeadband(m));
  m.AddCommand(new FeeServer::SetIssueTimeout(m));
  m.AddCommand(new FeeServer::GetIssueTimeout(m));
  m.AddCommand(new FeeServer::SetUpdateRate(m));
  m.AddCommand(new FeeServer::GetUpdateRate(m));
  m.AddCommand(new FeeServer::SetLogLevel(m));
  m.AddCommand(new FeeServer::GetLogLevel(m));

  Examples::TestControlEngine ce(m);
  m.SetCtrlEngine(ce);

  std::cout << "Running fee server " << std::endl;
  return m.Run();
}
//
// EOF
//
