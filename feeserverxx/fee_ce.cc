#include <feeserverxx/fee_ce.hh>
#include <feeserverxx/fee_main.hh>
#include <feeserverxx/fee_msg.hh>
#include <iostream>
#include <unistd.h>

//____________________________________________________________________
FeeServer::ControlEngine::ControlEngine(Main& m)
  : Thread("Control Engine"), 
    _main(m), 
    _mutex("ce_mutex"),
    _condition(),
    _is_ready(false), 
    _can_continue(false)
{}

//____________________________________________________________________
void*
FeeServer::ControlEngine::operator()() 
{
  // Enable cancels 
  Thread::Return ret = Thread::Success;
  if ((ret = EnableCancel(true)) != Success) 
    _main.Msg(Message::Warning, "ControlEngine", 
	      "Unable to configure CE thread for cancellation: %d", ret);

  // Cancel immediately
  if ((ret = AsyncCancel(true)) != Success) 
    _main.Msg(Message::Warning, "ControlEngine", 
	      "Unable to configure CE thread for async cancels %d", ret);

  // A nice message 
  _main.Msg(Message::Info, "", "Started CE thread");

  // Start the CE
  Start();

  return 0;
}

//____________________________________________________________________
void
FeeServer::ControlEngine::Ready()
{
  if (_is_ready) return;
#ifndef ARM_TARGET
  if (!IsCurrent()) {
    _main.Msg(Message::Info, "CE", "Must call Ready from CE thread!");
    return;
  }
#endif
  {
    Guard g(_mutex);
    // Guard g(_mutex);
    _is_ready = true;
    _condition.Broadcast();
  }
  usleep(10);
  Guard g(_mutex);
  while(!_can_continue) {
    _main.Msg(Message::Info, "CE", "waiting for init to finish");
    _condition.Wait(_mutex);
  }
  _main.Msg(Message::Info, "CE", "now ready to rock'n'roll");
}

//____________________________________________________________________
//
// EOF
//

