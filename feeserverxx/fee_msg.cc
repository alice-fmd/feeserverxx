#include <feeserverxx/fee_msg.hh>
#include <dim/dis.hxx>
#include <sstream>
#include <cstdarg>
#include <cstdio>
#include <iostream>

//____________________________________________________________________
FeeServer::MessageChannel::MessageChannel(const std::string& server, 
					  const std::string& detector,
					  unsigned long mask) 
  : _msg(), 
    _service(0), 
    _mutex("message_mutex"),
    _last(),
    _server(server), 
    _repeats(0), 
    _log_mask(mask | Message::Alarm),
    _buf(sizeof(int)
	 +Message::DetectorSize
	 +Message::SourceSize
	 +Message::DescriptionSize
	 +Message::DateSize)
{
  size_t i = 0;
  for (; i < std::min(detector.size(),size_t(Message::DetectorSize-1)); i++)
    _msg._detector[i] = detector[i];
  for (; i < Message::DetectorSize; i++) _msg._detector[i] = '\0';

  std::stringstream name;
  name << server << "_Message";
  
  std::stringstream format;
  format << "I:1;"
	 << "C:" << sizeof(_msg._detector)    << ";"
	 << "C:" << sizeof(_msg._source)      << ";"
	 << "C:" << sizeof(_msg._description) << ";" 
	 << "C:" << sizeof(_msg._date);
  
  
  _service = new DimService(const_cast<char*>(name.str().c_str()), 
			    const_cast<char*>(format.str().c_str()), 
			    reinterpret_cast<void*>(&(_buf[0])), 
			    _buf.size());
}

//____________________________________________________________________
FeeServer::MessageChannel::~MessageChannel()
{
  if (_service) delete _service;
}

//____________________________________________________________________
void
FeeServer::MessageChannel::HandleRepeats() 
{
  if (_repeats <= 0) return;
  
  std::string w(_msg._source);
  w.erase(0, _server.size());
  if (w[0] == '/') w.erase(0,1);
  UnlockedSend(Event(_msg._event_type), w, 
	       "Log message repeated %d times: %s", 
	       _repeats, _msg._description);
}

//____________________________________________________________________
void
FeeServer::MessageChannel::Send(Event event, const std::string& where, 
				const char* format, ...)
{
  Guard g(_mutex);
  va_list ap;
  va_start(ap, format); 
  char buf[sizeof(_msg._description)];
  size_t l = vsnprintf(buf, sizeof(buf)-1, format, ap);
  for (size_t i = l; i < sizeof(buf); i++) buf[i] = '\0';
  va_end(ap);

  DoSend(event, where, buf);
}

//____________________________________________________________________
void
FeeServer::MessageChannel::UnlockedSend(Event event, const std::string& where, 
					const char* format, ...)
{
  va_list ap;
  va_start(ap, format); 
  char buf[sizeof(_msg._description)];
  size_t l = vsnprintf(buf, sizeof(buf)-1, format, ap);
  for (size_t i = l; i < sizeof(buf); i++) buf[i] = '\0';
  va_end(ap);

  DoSend(event, where, buf);
}
  

//____________________________________________________________________
void
FeeServer::MessageChannel::DoSend(Event event, const std::string& where, 
				  const std::string& msg)
{
  const size_t desc_size = sizeof(_msg._description);
  const size_t src_size  = sizeof(_msg._source);
  // const size_t date_size = sizeof(_msg._date);
  if (!CheckEvent(event)) return;

  // Set the type of event
  _msg._event_type = event;

  // Copy message 
  strncpy(_msg._description, msg.c_str(), std::min(msg.size(),desc_size));
  for (size_t i = std::min(msg.size(),desc_size); i < desc_size; i++) 
    _msg._description[i] = '\0';

  // Check if this is a repeated message
  if (_last == _msg._description) {
    _repeats++;
    return;
  }
  
  // OK, this is new 
  _repeats = 0;
  _last    = _msg._description; 

  // Set the source 
  std::stringstream src;
  src << _server;
  if (!where.empty()) src << "/" << where;
  std::string s = src.str();
  strncpy(_msg._source, s.c_str(), std::min(src_size,s.size()));
  for (size_t i = std::min(src_size,s.size()); i < src_size; i++) 
    _msg._source[i] = '\0';

  // Create the date field 
  time_t     epoch_now = time(NULL);
  struct tm* local_now = localtime(&epoch_now);
  strftime(_msg._date, sizeof(_msg._date), "%Y-%m-%d %H:%M:%S",local_now);
  _msg._date[sizeof(_msg._date)-1] = '\0';

  std::cout << Message::Code2String(_msg._event_type) << "\t["
	    << _msg._date << "] from " << _msg._detector 
	    << "::" << _msg._source << ": " << _msg._description 
	    << std::endl;
  /// std::cout << _msg << std::endl;
  _msg.ToBuffer(_buf);
  // We shouldn't explicitly update - it will dead-lock
  // _service->updateService();
}

//====================================================================
void*
FeeServer::MessageWatchdog::operator()()
{
  // int status     = -1;
  // int sleep_sec  = 0;
  // int sleep_msec = 0;
  // Thread::Return ret = Thread::Success;
  /* ret = */ EnableCancel(true);
  /* ret = */ AsyncCancel(true);
  
  while (true) {
    { 
      Guard g(_message_channel._mutex); 
      _message_channel.HandleRepeats();
    }
    TestCancel();

    // Sleep a while 
    int sleep_sec  = _timeout / 1000;
    int sleep_msec = _timeout % 1000;
    usleep(sleep_msec * 1000);
    dtq_sleep(sleep_sec);

    TestCancel();
  }
  return 0;
}
  
//
// EOF
//

