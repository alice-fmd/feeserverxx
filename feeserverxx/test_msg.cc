#include <feeserverxx/fee_msg.hh>
#include <dim/dis.hxx>
#include <iostream>

const char* msgs[] = { "foo", "bar", "bar", "baz", "gnus" };
int 
main()
{
  FeeServer::MessageChannel msg("test", "foo");
  DimServer::setDnsNode("localhost");
  DimServer::start("test"); 
  
  FeeServer::MessageWatchdog w(msg);
  w.Run();

  while(1) { 
    std::cout << "please hit enter to update the service: " << std::flush;
    // char c = 
    std::cin.get();

    int type = 7 * float(rand()) / RAND_MAX;
    FeeServer::Message::Event event =  FeeServer::Message::Info;
    switch (type) {
    case 0:  event = FeeServer::Message::Info;         break;
    case 1:  event = FeeServer::Message::Warning;      break;
    case 2:  event = FeeServer::Message::Error;        break;
    case 3:  event = FeeServer::Message::AuditFailure; break;
    case 4:  event = FeeServer::Message::AuditSuccess; break;
    case 5:  event = FeeServer::Message::Debug;        break;
    case 6:  event = FeeServer::Message::Alarm;        break;
    }
    int id = 5 * float(rand()) / RAND_MAX;
    const char* m = msgs[id];
    std::cout << "Type: " << event << " Message: " << m << std::endl;
    msg.Send(event, "program", m);
  }
  return 0;
}
