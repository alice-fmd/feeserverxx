#include <feeserverxx/fee_cond.hh>
#include <pthread.h>
#include <cstdlib>
#include <cerrno>
#include <sys/time.h>
#include <ctime>

//====================================================================
FeeServer::Condition::Attributes::Attributes()
  : _attr()
{
  pthread_condattr_init(&_attr);
}

//____________________________________________________________________
FeeServer::Condition::Attributes::~Attributes()
{
  pthread_condattr_destroy(&_attr);
}

//====================================================================
FeeServer::Condition::Condition()
  : _cond()
{
  pthread_cond_init(&_cond, NULL);
}

//____________________________________________________________________
FeeServer::Condition::Condition(const Attributes& attr)
  : _cond()
{
  pthread_cond_init(&_cond, &(attr._attr));
}

//____________________________________________________________________
FeeServer::Condition::~Condition()
{
  pthread_cond_destroy(&_cond);
}


//____________________________________________________________________
const char*
FeeServer::Condition::Return2String(Return ret) 
{
  switch (ret) {
  case Success:    return "Success";
  case TimeOut:    return "Time-out";
  case Interrupt:  return "Interrupt";
  case Busy:       return "Busy";
  case BadTime:    return "Bad time";
  }
  return 0;
}

//____________________________________________________________________
FeeServer::Condition::Return
FeeServer::Condition::HandleReturn(int ret) const
{
  switch (ret) {
  case ETIMEDOUT: return TimeOut;
  case EINTR:     return Interrupt;
  case EBUSY:     return Busy;
  case EFAULT:    return BadTime;
  case EINVAL:    return BadTime;
  }
  return Success;
}

//____________________________________________________________________
FeeServer::Condition::Return
FeeServer::Condition::Signal()
{
  return HandleReturn(pthread_cond_signal(&_cond));
}

//____________________________________________________________________
FeeServer::Condition::Return
FeeServer::Condition::Broadcast()
{
  return HandleReturn(pthread_cond_broadcast(&_cond));
}

//____________________________________________________________________
FeeServer::Condition::Return
FeeServer::Condition::Wait(Mutex& mutex)
{
  return HandleReturn(pthread_cond_wait(&_cond, &(mutex._mutex)));
}

//____________________________________________________________________
FeeServer::Condition::Return
FeeServer::Condition::Wait(Mutex& mutex, const struct timespec& absolute_time)
{
  return HandleReturn(pthread_cond_timedwait(&_cond, &(mutex._mutex), 
					     &absolute_time));
}

//____________________________________________________________________
FeeServer::Condition::Return
FeeServer::Condition::Wait(Mutex& mutex, unsigned int timeout)
{
  struct timeval  now;
  struct timespec abs_time;
  if (gettimeofday(&now, 0) != 0) return BadTime;
  abs_time.tv_sec  = now.tv_sec + int(timeout / 1000);
  abs_time.tv_nsec = now.tv_usec * 1000 + (timeout % 1000) * 1000000;
  return Wait(mutex, abs_time);
}
//____________________________________________________________________
//
// EOF
//
