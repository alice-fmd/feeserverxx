#include <feeserverxx/fee_scmd.hh>
#include <feeserverxx/fee_msg.hh>
#include <feeserverxx/fee_main.hh>
#include <feeserverxx/fee_mon.hh>
#include <feeserverxx/fee_item.hh>
#include <fstream>
#include <iostream>
#include <cstring>
#include <cstdlib>

//====================================================================
FeeServer::ServerCommand::ServerCommand(Main& m, unsigned short mask) 
  : _mask(mask), 
    _main(m)
{}

//====================================================================
namespace FeeServer 
{
  //__________________________________________________________________
  short
  UpdateServer::Handle(const byte_array& idata, 
		       const size_t                      isize, 
		       byte_array&       /*odata*/, 
		       size_t&                           osize)
  {
    // std::cout << "Executing UpdateServer ... " << std::flush;
    // Zero output - just in case
    osize = 0;

    // Check that we got data
    if (isize == 0) {
      _main.Msg(Message::Error, "UpdateServer", "no data");
      return -Header::NullPointer;
    }

    // Open file 
    std::ofstream out("newFeeServer");
    if (!out) {
      _main.Msg(Message::Error,"UpdateServer", "unable to open output");
      return -Main::Error;
    }
    // Output to the file 
    for (size_t i = 0; i < isize; i++) out.put(idata[i]);
    out.close();
    _main.Msg(Message::Info,"UpdateServer", "server updated");

    // Exit status "3" tells the starting script to finalise update
    // and restart FeeServer after termination.
    _main.Stop(Main::Update);

    // Never reached 
    // std::cout << "done" << std::endl;
    return 0;
  }
  //__________________________________________________________________
  short
  RestartServer::Handle(const byte_array& /*idata*/, 
			const size_t                      /*isize*/, 
			byte_array&       /*odata*/, 
			size_t&                           osize)
  {
    // std::cout << "Executing RestartServer ... " << std::flush;
    // Zero output - just in case
    osize = 0;
    // Do a restart 
    _main.Stop(Main::Restart);
    // Return ok.
    // std::cout << "done" << std::endl;
    return 0;
  }
  //__________________________________________________________________
  short
  RebootMachine::Handle(const byte_array& /*idata*/, 
			const size_t                      /*isize*/, 
			byte_array&       /*odata*/, 
			size_t&                           osize)
  {
    // std::cout << "Executing RebootMachine ... " << std::flush;
    // Zero output - just in case
    osize = 0;
    _main.Msg(Message::Info, "RebootMachine", "Rebooting DCS card");
    system("reboot");
    // std::cout << "done" << std::endl;
    return 0;
  }
  //__________________________________________________________________
  short
  ShutdownMachine::Handle(const byte_array& /*idata*/, 
			  const size_t                      /*isize*/, 
			  byte_array&       /*odata*/, 
			  size_t&                           osize)
  {
    // std::cout << "Executing ShutdownMachine ... " << std::flush;
    // Zero output - just in case
    osize = 0;
    _main.Msg(Message::Info, "ShutdownMachine", "Shutting down DCS card");
    system("poweroff");
    // std::cout << "done" << std::endl;
    return 0;
  }
  //__________________________________________________________________
  short
  ExitServer::Handle(const byte_array& /*idata*/, 
		     const size_t                      /*isize*/, 
		     byte_array&       /*odata*/, 
		     size_t&                           osize)
  {
    // std::cout << "Executing ExitServer ... " << std::flush;
    // Zero output - just in case
    osize = 0;
    _main.Stop(0);
    // std::cout << "done" << std::endl;
    return 0;
  }
  //__________________________________________________________________
  short
  SetDeadband::Handle(const byte_array& idata, 
		      const size_t                      isize, 
		      byte_array&       /*odata*/, 
		      size_t&                           osize)
  {
    // std::cout << "Executing SetDeadband ... " << std::flush;
    osize = 0;
    size_t float_size = sizeof(float);

    // Check message 
    if (isize <= float_size) {
      _main.Msg(Message::Debug, "SetDeadband", "not enough data");
      return -Header::InvalidParam;
    }
    size_t name_len = isize - float_size;
    if (name_len <= 0) {
      _main.Msg(Message::Debug, "SetDeadband", "no service name");
      return  -Header::InvalidParam;
    }
      
    // Get the values. 
    float new_val;
    std::string name;
    memcpy(&new_val, &(idata[0]), float_size);
    for (size_t i = float_size; i < isize; i++) name.push_back(idata[i]);
    
    // counter 
    size_t count = 0;

    // Check if we're broadcasting 
    if (name[0] == '*') {
      // find substring starting with '_'
      size_t i = name.find('_');
      if (i == std::string::npos) return 0;
      std::string sub_name = name.substr(i, name.size()-i);
      
      for (MonitorThread::ItemMap::iterator j = 
	     _main.MonThread().Items().begin(); 
	   j != _main.MonThread().Items().end(); ++j) {
	// find substring starting with '_'
	const std::string& iname = j->second->Name();
	i = iname.find('_');
	if (i == std::string::npos) continue;

	// Check if names match
	if (sub_name != iname.substr(i, iname.size()-i)) continue;

	// Set the new dead band 
	j->second->SetDeadband(new_val / 2);
	count++;
      }
    }
    else {
      MonitorThread::ItemMap::iterator i = 
	_main.MonThread().Items().find(name);
      if (i == _main.MonThread().Items().end()) { 
	_main.Msg(Message::Warning, "SetDeadband", "Item %s not found", 
		  name.c_str());
	_main.Msg(Message::Warning, "SetDeadband", "invalid parameter");
	return -Header::InvalidParam;
      }
      i->second->SetDeadband(new_val / 2);
      count++;
      _main.Msg(Message::Info, "SetDeadband", "new dead band (%f) for item %s",
		new_val, name.c_str());
    }
    _main.Msg(Message::Info, "SetDeadband", 
	      "updated dead-bands for %d services", count);
    // std::cout << "done" << std::endl;
    return 0;
  }
  //__________________________________________________________________
  short
  GetDeadband::Handle(const byte_array& idata, 
		      const size_t                      isize, 
		      byte_array&       odata, 
		      size_t&                           osize)
  {
    // std::cout << "Executing GetDeadband ... " << std::flush;
    osize = 0;
    size_t float_size = sizeof(float);

#if 0
    // Check message 
    if (isize <= float_size) {
      _main.Msg(Message::Debug, "GetDeadband", "not enough data");
      return -Header::InvalidParam;
    }
#endif
    // Get the name
    std::string name;
    for (size_t i = 0; i < isize; i++) name.push_back(idata[i]);

    // Find item.
    MonitorThread::ItemMap::iterator i = _main.MonThread().Items().find(name);
    if (i == _main.MonThread().Items().end()) { 
      _main.Msg(Message::Warning, "SetDeadband", "Item %s not found", 
		name.c_str());
      _main.Msg(Message::Warning, "SetDeadband", "invalid parameter");
      return -Header::InvalidParam;
    }
    float val = i->second->GetDeadband() * 2;
    
    if (odata.size() < float_size+name.size()+1) 
      odata.resize(float_size+name.size()+1);
    memcpy(&(odata[0]), &val, float_size);
    memcpy(&(odata[float_size]), &(name.c_str()[0]), name.size());
    odata[float_size+name.size()] = '\0';
    osize = float_size+name.size()+1;

    // std::cout << "done" << std::endl;
    return 0;
  }
  //__________________________________________________________________
  short
  SetIssueTimeout::Handle(const byte_array& idata, 
			  const size_t                      isize, 
			  byte_array&       /*odata*/, 
			  size_t&                           osize)
  {
    // std::cout << "Executing SetIssueTimeout ... " << std::flush;
    osize = 0;
    size_t long_size = sizeof(unsigned long);

    // Check message 
    if (isize < long_size) {
      _main.Msg(Message::Debug, "SetIssueTimeout", "not enough data");
      return -Header::InvalidParam;
    }
    unsigned long new_val;
    memcpy(&new_val, &(idata[0]), long_size);
    if (new_val > 4294967) {
      _main.Msg(Message::Warning, "SetIssueTimeout", "too large a number");
      return -Header::InvalidParam;
    }
    _main.SetIssueTimeout(new_val);
    _main.Msg(Message::Info, "SetIssueTimeout", 
			    "Issue timeout set to %lu milliseconds", new_val);
    
    // std::cout << "done (" << new_val << ")" << std::endl;
    return 0;
  }
  //__________________________________________________________________
  short
  GetIssueTimeout::Handle(const byte_array& /*idata*/, 
			  const size_t                      /*isize*/, 
			  byte_array&       odata, 
			  size_t&                           osize)
  {
    // std::cout << "Executing GetIssueTimeout ... " << std::flush;
    size_t long_size = sizeof(unsigned long);
    unsigned long val = _main.GetIssueTimeout();
    if (odata.size() < long_size) odata.resize(long_size);
    memcpy(&(odata[0]), &val, long_size);
    osize = long_size;
    _main.Msg(Message::Debug, "GetIssueTimeout", 
			    "Issue timeout is %lu milliseconds", val);

    // std::cout << "done (" << val << ")" << std::endl;
    return 0;
  }
  //__________________________________________________________________
  short
  SetUpdateRate::Handle(const byte_array& idata, 
			const size_t                      isize, 
			byte_array&       /*odata*/, 
			size_t&                           osize)
  {
    // std::cout << "Executing SetUpdateRate ... " << std::flush;
    osize = 0;
    size_t short_size = sizeof(unsigned short);

    // Check message 
    if (isize < short_size) {
      _main.Msg(Message::Debug, "SetUpdateRate", "not enough data");
      return -Header::InvalidParam;
    }
    unsigned short new_val;
    memcpy(&new_val, &(idata[0]), short_size);
    _main.SetUpdateRate(new_val);
    _main.Msg(Message::Info, "SetUpdateRate", 
			    "update rate set to %lu milliseconds", new_val);
    
    // std::cout << "done" << std::endl;
    return 0;
  }
  //__________________________________________________________________
  short
  GetUpdateRate::Handle(const byte_array& /*idata*/, 
			const size_t                      /*isize*/, 
			byte_array&       odata, 
			size_t&                           osize)
  {
    // std::cout << "Executing GetUpdateRate ... " << std::flush;
    size_t short_size = sizeof(unsigned short);
    unsigned short val = _main.GetUpdateRate();
    if (odata.size() < short_size) odata.resize(short_size);
    memcpy(&(odata[0]), &val, short_size);
    osize = short_size;
    // #if 0
    _main.Msg(Message::Debug, "GetUpdateRate", 
			    "update rate is %u milliseconds", val);
    // #endif
    // std::cout << "done (" << val << ")" << std::endl;
    return 0;
  }
  //__________________________________________________________________
  short
  SetLogLevel::Handle(const byte_array& idata, 
		      const size_t                      isize, 
		      byte_array&       /*odata*/, 
		      size_t&                           osize)
  {
    // std::cout << "Executing SetLogLevel ... " << std::flush;
    osize = 0;
    size_t int_size = sizeof(unsigned int);

    // Check message 
    if (isize < int_size) {
      _main.Msg(Message::Debug, "SetLogLevel", "not enough data");
      return -Header::InvalidParam;
    }
    unsigned int new_val;
    memcpy(&new_val, &(idata[0]), int_size);
    new_val |= Message::Alarm;
    // _main.SetLogLevel(new_val);
    _main.Msg(Message::Info, "SetLogLevel", 
			    "log level set to  0x%x", new_val);
    
    // std::cout << "done (" << new_val << ")" << std::endl;
    return 0;
  }
  //__________________________________________________________________
  short
  GetLogLevel::Handle(const byte_array& /*idata*/, 
		      const size_t                      /*isize*/, 
		      byte_array&       odata, 
		      size_t&                           osize)
  {
    // std::cout << "Executing GetLogLevel ... " << std::flush;
    size_t int_size = sizeof(unsigned int);
    unsigned int val = _main.GetLogLevel();
    if (odata.size() < int_size) odata.resize(int_size);
    memcpy(&(odata[0]), &val, int_size);
    osize = int_size;
#if 0
    osize = 0;
    _main.Msg(Message::Debug, "GetLogLevel", 
			    "log level is 0x%x", val);
#endif
    // std::cout << "done (" << val << ")" << std::endl;
    return 0;
  }
  //__________________________________________________________________
  short
  ShowServices::Handle(const byte_array& /*idata*/, 
		       const size_t                      /*isize*/, 
		       byte_array&       /*odata*/, 
		       size_t&                           osize)
  {
    // std::cout << "Executing SetDeadband ... " << std::flush;
    osize = 0;
    std::cout << "Monitored services: " << std::endl;
    for (MonitorThread::ItemMap::iterator j=_main.MonThread().Items().begin(); 
	 j != _main.MonThread().Items().end(); ++j) 
      std::cout << "\t" << (j->first) <<  "\t" << (j->second) << std::endl;
    return 0;
  }
#if 0
  //__________________________________________________________________
  short
  SuspendMonitoring::Handle(const byte_array& /*idata*/, 
			    const size_t                      /*isize*/, 
			    byte_array&       /*odata*/, 
			    size_t&                           osize)
  {
    // std::cout << "Executing SetDeadband ... " << std::flush;
    osize = 0;
    _main.MonThread().Suspend();
  }
  //__________________________________________________________________
  short
  ResumeMonitoring::Handle(const byte_array& /*idata*/, 
			    const size_t                      /*isize*/, 
			    byte_array&       /*odata*/, 
			    size_t&                           osize)
  {
    // std::cout << "Executing SetDeadband ... " << std::flush;
    osize = 0;
    _main.MonThread().Resume();
  }
#endif
}

//
// EOF
//

