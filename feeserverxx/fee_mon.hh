#ifndef FEE_MON_HH
#define FEE_MON_HH
#include <feeserverxx/fee_thread.hh>
#include <feeserverxx/fee_lock.hh>
#include <feeserverxx/fee_cond.hh>
#include <map>

namespace FeeServer 
{
  // forward declaration 
  class Main;
  class ServiceBase;
  class Item;

  //__________________________________________________________________
  /** @class MonitorThread fee_mon.hh <feeserverxx/fee_mon.hh> 
      @brief Thread that updates published services 
      @ingroup processes 
  */
  class MonitorThread : public Thread 
  {
  public:
    /** Map of items */ 
    typedef std::map<std::string, Item*> ItemMap;
    /** Constructor 
	@param main Reference to fee server main. 
	@param rate Update rate in miliseconds. */
    MonitorThread(Main& main, unsigned int rate=1000);
    /** Do the thread thing */
    void* operator()();
    /** Suspend the thread.  Must not be called from monitor thread it
	self, as it would dead-lock the thread. 
	@return true on succes, false otherwise */
    bool Suspend();
    /** Resume the thread.  Must not be called from monitor thread it
	self, as it would dead-lock the thread. 
	@return true on succes, false otherwise */
    bool Resume();
    /** Set the update rate */ 
    void SetRate(unsigned int rate) { _update_rate = rate; }
    /** Get the update rate */
    unsigned int Rate() const { return _update_rate; }
    /** Add a service
	@param service Service to publish 
	@return 0 on success, negativ erroe code otherwise */
    int Publish(ServiceBase& service);
    /** Un-publish a service.  It is not removed from the list, but
	will not be updated 
	@param name Name of service to remove 
	@return 0 on success, negativ erroe code otherwise */
    int Unpublish(const std::string& name);
    /** Remove all services */ 
    void RemoveServices();
    /** Get item map */
    ItemMap& Items() { return _items; }
    /** 
     * Check if the monitor is suspended. 
     * 
     * @return @c true if the monitor thread is suspended
     */
    bool IsSuspended() const { return _suspend; }
  protected: 
    /** Reference to server. */
    Main& _main;
    /** Items */
    ItemMap _items;
    /**  Update rate, in which the whole Item-list should be checked
	 for changes. This value is given in milliseconds. */
    unsigned int _update_rate;
    /** Mutex for suspension */
    Mutex _mutex;
    /** Condition for suspension */
    Condition _condition;
    /** Flag to indicate suspension */
    bool _suspend;
    /** The service list is changed - restart inner loop. */
    bool _changed;
    /** Interval multiplier */
    const unsigned int _interval_multiplier;


  };
}

#endif
//
// EOF
//

