#ifndef FEE_MAIN_HH
#define FEE_MAIN_HH
#include <string>
#include <vector>
#include <map>
#include <feeserverxx/fee_lock.hh>
#include <feeserverxx/fee_cond.hh>
#include <feeserverxx/fee_issue.hh>
#include <feeserverxx/fee_mon.hh>
#include <feeserverxx/fee_packet.hh>
#include <feeserverxx/fee_msg.hh>
/**
 * @file   fee_main.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Apr 29 22:11:17 2009
 * 
 * @brief  Declaration of FeeServer::Main
 * 
 * 
 */

namespace FeeServer 
{
  // Forward declarations 
  class AcknowledgeChannel;
  class CommandChannel;
  class Command;
  class Acknowledge;
  class ServerCommand;
  class ControlEngine;
  class ServiceBase;
  class ErrorExitHandler;

  //__________________________________________________________________
  /** @defgroup core Core classes. 
      This group contains the core classes of the fee server */
  
  //__________________________________________________________________
  /** @defgroup processes Processes
      @ingroup core
      This group contains the processes run by the core. */

  //__________________________________________________________________
  /** @class Main fee_main.hh <feeserverxx/fee_main.hh> 
      @brief The Feeserver 
      Main manager of of the fee server. 
      @ingroup core 
  */
  class Main 
  {
  public:
    enum {
      /** return value that restart the server */ 
      Restart = 2, 
      /** Return value that updates and restarts the server */ 
      Update = 3, 
      /** Return value that says we should restart since init failed */
      InitRestart = 4,
      /** Dim error */
      DimExit = 204
    };

    /** States of the fee server */
    enum State {
      /** Collecting information */
      Collecting,
      /** Running */
      Running, 
      /** In error */
      Error
    };

    /** Constructor 
	@param name Name of server 
	@param detector name of detector (max 3 letters) 
	@param dns  DIM domain name server host */
    Main(const std::string& name, 
	 const std::string& detector, 
	 const std::string& dns);
    /** Destructor */
    virtual ~Main();

    /** Get the static instance */ 
    static Main& Instance();

    /** Get the name */
    const std::string& Name() const { return _name; }
    /** Get the version of the package */
    const char* VersionString() const;
    /** Get the name of the package */
    const char* PackageString() const;

    /** Get the version string of this library */
    static const std::string& Version();
    /** Get the package name of this library */
    static const std::string& Package();
    
    /** @{ 
	@name Steering routines */
    /** Run the fee server 
	@param ce_timeout Time out for CE to start (milliseconds)
	@return 0 on succces, error code otherwise */
    int Run(const unsigned int ce_timeout=2000);
    /** Clean up all */
    void CleanUp();
    /** Stop the server 
	@param ret is the return value */
    void Stop(int ret);
    /** 
     * Handle a signal sent to application
     * 
     * @param sig 
     */
    void HandleSignal(int sig);
    /** 
     * Install signal handlers
     * 
     */    
    void InstallHandlers();
    /** @} */

    /** @{ 
	@name Init/CE thread handling */
    /** Set the control engine */
    void SetCtrlEngine(ControlEngine& e) { _control_engine = &e; }
    /** Get reference to the control engine */ 
    ControlEngine& CtrlEngine() { return *_control_engine; }
    /** Get reference to the control engine */ 
    const ControlEngine& CtrlEngine() const { return *_control_engine; }
    /** @} */

    /** @{ 
	@name Message handling */
    /** Get a reference to the message channel 
	@return reference to the message channel */
    MessageChannel& MsgChannel() { return *_msg_channel; }
    /** Make a message.  If the message channel isn't up and running
	yet, we simply print to standard out. 
	@param event   Event type 
	@param where   Where the message came from 
	@param format  @c printf like format 
    */
    void Msg(Message::Event event, const std::string& where, 
	     const char* format, ...);
    /** Set the log-level */
    void SetLogLevel(unsigned int mask);
    /** Get the log mask */
    unsigned int GetLogLevel() const;
    /** Set the watchdog time out */
    void SetWatchdogTimeout(unsigned int timeout);
    /** Set the watchdog time out */
    unsigned int GetWatchdogTimeout() const;
    /** @} */

    /** @{ 
	@name Server commands */
    /** Handle commands arriving over the command channel */ 
    void HandleCommand(Command& c);
    /** Add a command to the known server commands */ 
    bool AddCommand(ServerCommand* c);
    /** Remove commands */ 
    void RemoveCommands();
    /** @} */

    /** @{ 
	@name Services */
    /** publish a service */ 
    int Publish(ServiceBase& service);
    /** Un-publish a service.  It is not removed from the list, but
	will not be updated */
    int Unpublish(const std::string& name);
    /** Remove all services */ 
    void RemoveServices();
    /** Set the update rate time out in milliseconds */
    void SetUpdateRate(unsigned short rate) { _mon_thread.SetRate(rate); }
    /** Get the update rate in milliseconds */
    unsigned short GetUpdateRate() const { return _mon_thread.Rate(); }
    /** 
     * Get the Monitor thread
     * 
     * 
     * @return Reference to monitor thread
     */
    MonitorThread& MonThread() { return _mon_thread; }
    /** 
     * Get the Monitor thread
     * 
     * 
     * @return Reference to monitor thread
     */
    const MonitorThread& MonThread() const { return _mon_thread; }
    /** @} */
    
    /** @{
	@name Misc */
    /** Get the state of the fee server */
    const State& CurrentState() const { return _state; }
    /** Set the issue time out in milliseconds */
    void SetIssueTimeout(unsigned long timeout) { _issue_timeout = timeout; }
    /** Get the issue timeout in milliseconds */
    unsigned long GetIssueTimeout() const { return _issue_timeout; }
    /** @} */
  protected: 
    /** Not implemented */ 
    Main(const Main&);
    /** Not implemented */ 
    Main& operator=(const Main&);
    /** Handler of DIM errors and exit requests is a friend */
    friend class ErrorExitHandler;
    /** Handle DIM errors and exit requests */
    ErrorExitHandler* _error_exit;
    /** Handle DIM errors */ 
    virtual void ErrorHandler(int severity, int code, char *msg);
    /** Handle DIM exit requests */
    virtual void ExitHandler(int code);

    /** Initialize 
	@param ce_timeout Time out for CE startup */
    void Init(const unsigned int ce_timeout=1000);

    /** The control engine */
    ControlEngine* _control_engine;

    /** Name of the server */
    std::string         _name;
    /** Name of the detector */
    std::string         _detector;
    /** The return value of the server */ 
    int _retval;

    /** State of the fee server */ 
    State _state;

    /** The message channel */
    MessageChannel*     _msg_channel;
    /** The acknowledge channel */
    AcknowledgeChannel* _ack_channel;
    /** The command channel */
    CommandChannel*     _cmd_channel;
    /** Message channel watch dog */
    MessageWatchdog*    _msg_watchdog;

    /** Acknowledge structure */ 
    Acknowledge _ack;

    /** Mutex to lock command handling */ 
    Mutex _command_mutex;
    /** Mutex to lock issue handling */ 
    Mutex _wait_mutex;
    /** Condition to wait for issue to finish */
    Condition _wait_condition;

    /** Attributes for issue thread */ 
    Thread::Attributes _detached_attr;

    /** Issue thread is a friend */ 
    friend class IssueThread; 
    /** Issue thread */ 
    IssueThread _issue_thread;
    /** Timeout for call of issue - the longest time a command can be 
	executed by the CE, before the watch dog kills this
	thread. This value is given in milliseconds. */
    unsigned long _issue_timeout;

    /** Monitor thread */
    MonitorThread _mon_thread;

    /** A list of server commands */
    typedef std::vector<ServerCommand*> CommandList;
    /** The list of known server commands */ 
    CommandList _commands;

    mutable unsigned long _msg_mask;
    mutable unsigned long _msg_timeout;

    /** Singleton instance */
    static Main* _instance;
  };
}

#endif
//
// EOF
//
