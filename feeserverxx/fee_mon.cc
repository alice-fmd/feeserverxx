#include <feeserverxx/fee_mon.hh>
#include <feeserverxx/fee_msg.hh>
#include <feeserverxx/fee_main.hh>
#include <feeserverxx/fee_item.hh>
#include <feeserverxx/fee_serv.hh>
#include <feeserverxx/fee_dbg.hh>
#include <iostream>
#include <unistd.h>

//====================================================================
FeeServer::MonitorThread::MonitorThread(Main& m, unsigned int rate) 
  : Thread("Monitor"), 
    _main(m), 
    _items(),
    _update_rate(rate), 
    _mutex("monitor_mutex"),
    _condition(),
    _suspend(false), 
    _changed(false),
    _interval_multiplier(25)
{}

//____________________________________________________________________
bool
FeeServer::MonitorThread::Suspend()
{
  if (IsCurrent()) {
    _main.Msg(Message::Warning, "Monitor", 
	      "monitor thread cannot suspend it self");
    return false;
  }
  if (_suspend) { 
    _main.Msg(Message::Info, "Monitor", 
	      "monitor thread already suspended");
    return true;
  }
  Guard g(_mutex);
  DMSG(1, "Suspending montitor thread");
  _suspend = true;
  return true;
}

//____________________________________________________________________
bool
FeeServer::MonitorThread::Resume()
{
  if (!_suspend) return true;
  if (IsCurrent()) {
    _main.Msg(Message::Warning, "Monitor", 
	      "monitor thread cannot resume it self");
    return false;
  }
  Guard g(_mutex);
  DMSG(1, "Resuming montitor thread");
  _suspend = false;
  _condition.Broadcast();
  return true;
}

//____________________________________________________________________
struct SuspendGuard 
{
  SuspendGuard(FeeServer::MonitorThread& m) 
    : _mon_thread(m),
      _suspended_on_entry(m.IsSuspended())

  {
    if (!_suspended_on_entry) _mon_thread.Suspend();
  }
  ~SuspendGuard() 
  {
    if (!_suspended_on_entry) _mon_thread.Resume();
  }
  FeeServer::MonitorThread& _mon_thread;
  bool _suspended_on_entry;
};

//____________________________________________________________________
int
FeeServer::MonitorThread::Publish(ServiceBase& service)
{
  _main.Msg(Message::Debug, "Monitor","Will publish service %p", &service);
  if (service.Name().empty()) {
    _main.Msg(Message::Warning, "Monitor", "No service name provided");
    return -Header::NullPointer;
  }
  if (!service.Address() || !service.Size()) { 
    _main.Msg(Message::Warning, "Monitor", 
	      "No service address or size provided for %s", 
	      service.Name().c_str());
    return -Header::NullPointer;
  }

  // Suspend the monitor thread 
  SuspendGuard sus(*this);
  
  // Check if we have the same item already 
  ItemMap::iterator i = _items.find(service.Name());
  if (i != _items.end() && i->second) {
    _main.Msg(Message::Warning, "Monitor", "Service %s already exists", 
	      service.Name().c_str());
  }
  else {
    DMSG(1, "Added new service %s", service.Name().c_str());
    _items[service.Name()] = new Item(_main.Name(), service);
    _changed = true;
  }
  return 0;
}

//____________________________________________________________________
int
FeeServer::MonitorThread::Unpublish(const std::string& name)
{
  if (name.empty()) return -Header::NullPointer;

  // Suspend the monitor thread 
  SuspendGuard sus(*this);

  // Find the item
  ItemMap::iterator i = _items.find(name);
  int ret = Header::InvalidParam;
  if (i != _items.end()) { 
    Item* serv = i->second;
    DMSG(1, "Removed service %s", name.c_str());
    _items.erase(i);
    // _items[name] = 0;
    _main.Msg(Message::Debug, "Monitor", 
	      "Removing service %s, now have %d services", name.c_str(), 
	      _items.size());
    if (serv) delete serv;
    ret = Header::Ok;
    _changed = true;
  }
  else {
    _main.Msg(Message::Warning, "Monitor", 
	      "Cannot remove %s, no such service", name.c_str());
  }
  // _mon_thread.Resume();
  return ret;
}

//____________________________________________________________________
void
FeeServer::MonitorThread::RemoveServices()
{
  SuspendGuard g(*this);
  for (ItemMap::iterator i = _items.begin(); i != _items.end(); ++i)
    if (i->second) delete i->second;
  _items.clear();
}

//____________________________________________________________________
void*
FeeServer::MonitorThread::operator()() 
{
  // Enable cancels 
  Thread::Return ret = Thread::Success;
  if ((ret = EnableCancel(true)) != Success) 
    _main.Msg(Message::Warning, "Monitor",
	      "Unable to configure monitor thread for cancellation: %d", ret);

  // Cancel immediately
  if ((ret = AsyncCancel(true)) != Success) 
    _main.Msg(Message::Warning, "Monitor", 
	      "Unable to configure monitor thread for async cancels: %d" ,ret);

  // A nice message 
  _main.Msg(Message::Info, "Monitor", "Started monitor thread");

  unsigned long inner = 0, outer = 0;
  while (true) {
    TestCancel();

    size_t   n_items    = _items.size();
    if (n_items == 0) {
      usleep(_update_rate * 1000);
      TestCancel();
      continue;
    }
    // std::cout << "Monitoring " << n_items << " values " << std::flush;
    unsigned sleep_time = (_update_rate / n_items);
    ItemMap::iterator i = _items.begin();
    while (i != _items.end()) {
      if (_changed) {
	DMSG(1, "List of services changed - restarting inner loop");
	Guard g(_mutex);
	// std::cout << "we're changed" << std::endl;
	_changed = false;
	break;
      }
      else { 
      // Check if we're asked to be suspended. 
	Guard g(_mutex);
	bool was_suspend = _suspend; 
	while (_suspend) {
	  DMSG(1, "Monitor thread suspended");
	  INFO(_main, ("Monitor is suspended"));
	  // _main.Msg(Message::Info, "Monitor", "Monitor is suspended");
	  _condition.Wait(_mutex);
	  INFO(_main, ("Monitor is resumed"));
	  // _main.Msg(Message::Info, "Monitor", "Monitor is
	  // resumed");
	  // Lt the iterator point to the begining.
	  i = _items.begin();
	  break;
	}
	TestCancel();
	if (was_suspend) {
	  DMSG(1, "Was suspended - restarting inner loop");
	  // break;
	}
      
	bool force = (outer >= inner * _interval_multiplier);
	if (i->second) { 
	  DMSG(20, "Updating %s", i->second->Name().c_str());
	  if (!(i->second->Update(force))) 
	    _main.Msg(Message::Error, "Monitor", 
		      "Value of item %s is corrupted", 
		      i->second->Name().c_str());
	}
	inner++;
	// Move on to the next item 
	++i; 
      }

      // Sleep a while
      DMSG(20, "Sleeping for %dms", sleep_time);
      usleep(sleep_time * 1000); 
      // std::cout << (_suspend ? "+" : ".") << std::flush;
    }
    // Due to the check on both counters, each service is updated at
    // least every (_update_rate * #_of_items) seconds;
    inner = 0;
    outer++;
    
    // After every service in the list have been updated, set counters
    // back to 0, the _interval_multiplier is used to increase the
    // time interval of the request of services without touching the
    // deadband check. 
    if (outer >= (n_items * _interval_multiplier)) outer = 0;
    // std::cout << "done" << std::endl;
  }
  return 0;
}

//
// EOF
//
