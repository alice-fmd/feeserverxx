#include <fee_item.hh>
#include <sstream>
#include <dim/dis.hxx>
#include <iostream>

//____________________________________________________________________
FeeServer::Item::Item(const std::string& server, ServiceBase& service) 
  : _service(service), _dim_service(0)
{
  std::stringstream s;
  s << server << "_" << _service.Name();

  std::string name = s.str();
#if 0
  std::cout << "Making DIM service " << name << " with format " 
	    << service.Format() << " @ " << _service.Address() 
	    << " of size " << _service.Size() << std::endl;
#endif
  _dim_service = new DimService(const_cast<char*>(name.c_str()), 
				const_cast<char*>(_service.Format()), 
				0, 0); 
  Update(true);
  // _service.Address(), _service.Size());
}

//____________________________________________________________________
FeeServer::Item::~Item()
{
  if (_dim_service) {
#if 0
    std::cout << "Deleting DIM service " << _dim_service->getName()
	      << std::endl;    
#endif
    delete _dim_service;
  }
}

//____________________________________________________________________
const std::string& 
FeeServer::Item::Name() const 
{
  return _service.Name();
}

//____________________________________________________________________
void
FeeServer::Item::SetDeadband(float v)
{
  _service.SetDeadband(v);
}

//____________________________________________________________________
float
FeeServer::Item::GetDeadband() const
{
  return _service.GetDeadband();
}

//____________________________________________________________________
bool
FeeServer::Item::Update(bool force)
{
  if (!_service.Updated() && !force) return true;
  /// if (!force || !_service.Updated()) return true;
  if (!_service.Consistency()) return false;
  _service.Clear();
  _dim_service->setTimestamp(time(NULL), 0);
  _dim_service->setQuality(_service.Quality());
  _dim_service->updateService(_service.Address(), _service.Size());
  return true;
}

//____________________________________________________________________
//
// EOF
//

