#ifndef FEE_COND_HH
#define FEE_COND_HH
#include <feeserverxx/fee_lock.hh>
#include <pthread.h>

namespace FeeServer 
{
  /** @class Condition fee_cond.hh <feeserverxx/fee_cond.hh>
      @brief Syncronisation point of threads 
      @ingroup threads 
  */
  class Condition 
  {
  public:
    /** @class Attributes fee_cond.hh <feeserverxx/fee_cond.hh>
	@brief Conditin attributes 
	@ingroup threads
    */
    class Attributes 
    {
    public:
      /** Constructor */ 
      Attributes();
      /** Destrcutor */ 
      virtual ~Attributes();
    protected: 
      /** Attributes */
      pthread_condattr_t _attr;
      /** Condition class is a friend */
      friend class Condition;
    };

    enum Return {
      // Everything is cool
      Success = 0, 
      // Wait timed out. 
      TimeOut, 
      // Interrupt was recieved 
      Interrupt, 
      // Condition is already busy 
      Busy, 
      // Bad time 
      BadTime
    };

    /** constructor */
    Condition();
    /** constructor */
    Condition(const Attributes& attr);
    /** Destructor */ 
    virtual ~Condition();
    /** Signal the condition 
	@return true on success, false otherwise */
    Return Signal();
    /** Broad cast the condition 
	@return true on success, false otherwise */
    Return Broadcast();
    /** Wait for condition to be signalled 
	@param mutex Must be locked on calling this member function.
	Will be unlocked and relocked when the condition is signalled 
	@return true on success, false otherwise */
    Return Wait(Mutex& mutex);
    /** Timed wait. 
	@param mutex Must be locked on calling this member function.
	Will be unlocked and relocked when the condition is signalled 
	@param absolute_time Absolute time (in seconds since epoch) to
	wait until or give up. 
	@return true on success, false otherwise */
    Return Wait(Mutex& mutex, const struct timespec& absolute_time); 
    /** Timed wait. 
	@param mutex Must be locked on calling this member function.
	Will be unlocked and relocked when condition is signaled. 
	@param timeout Time from now to time out in milliseconds.
	@return Return value */
    Return Wait(Mutex& mutex, unsigned int timeout);
    
    static const char* Return2String(Return ret);
  protected:
    /** Convert return values */
    Return HandleReturn(int ret) const;
    /** Condition */
    pthread_cond_t _cond;
  };
}

#endif
//
// EOF
//
