#ifndef FEE_THREAD_HH
#define FEE_THREAD_HH
#include <pthread.h>
#include <string>
#include <map>
#include <feeserverxx/fee_lock.hh>

namespace FeeServer 
{
  /** 
   * @defgroup threads Threading classes 
   * This group defines a number of threading classes and utility
   * functions.  
   */
 
  /** 
   * Clean-up handler 
   * @ingroup threads 
   */
  void fee_thread_cleanup(void* arg);

  /** 
   * Trampoline function 
   * @ingroup threads 
   */
  void* fee_thread_launcher(void* arg) throw ();
  // Forward decl 
  class ThreadManager;

  /** 
   * @class Thread fee_thread.hh <feeserverxx/fee_thread.hh> 
   * @brief Class representing a thread.  The user should overload
   * the function operator member function (@c operator()) to do the
   * thread job.   Provides member function for cancelling, joining,
   * and so on. 
   * @ingroup threads 
   */
  class Thread 
  {
  public:
    /** 
     * @class Attributes fee_thread.hh <feeserverxx/fee_thread.hh> 
     * @brief Thread attributes 
     * @ingroup threads
     */
    class Attributes 
    { 
    public:
      /** 
       * Scheduling priorities 
       */
      enum Scheduling { 
	Other, 
	RoundRobin, 
	Fifo 
      };
      /** 
       * @param detached Wether to start in detached state 
       * @param inherit  Inherit scheduling parameters and policies 
       * @param policy   Scheduling policy 
       * @param priority Scheduling priority 
       */
      Attributes(bool detached=false, bool inherit=true, 
		 Scheduling policy=Other, int priority=0);
      /** Destructor */
      virtual ~Attributes();
      /** 
       * @param detach Wether to start in detached state  
       */
      bool Detach(bool detach);
      /** 
       * @param inherit Inherit scheduling parameters and policies 
       */
      bool Inherit(bool inherit); 
      /** 
       * @param policy Scheduling policy 
       * @param priority Scheduling priority 
       */
      bool Schedule(Scheduling policy, int priority=0);
      /** 
       * @return true if threads are started detached 
       */
      bool Detached() const;
      /** 
       * @return true if threads inherits scheduling parameters 
       */ 
      bool Inherits() const;
      /** 
       * @return the policy type 
       */
      Scheduling Policy() const;
      /** 
       * @return the priority 
       */
      int Priority() const;
    protected: 
      /** Thread is a friend */
      friend class Thread;
      /** Attributes */
      pthread_attr_t _attr;
    };

    /** 
     * Return codes 
     */
    enum Return  {
      /** Success */
      Success, 
      /** Missing system resources */ 
      Missing,
      /** No such thread */
      BadThread, 
      /** Operation not meaningful for thread */ 
      BadValue, 
      /** Would dead-lock */
      DeadLock,
      /** Thread is already running */
      Running, 
      /** Calling thread isn't this thread */ 
      NotCurrent,
      /** Calling thread isn't running */
      NotRunning
    };

    /** 
     * Constructor.  
     * @param name Name of string 
     */
    Thread(const std::string& name=std::string());
    /** Destructor */ 
    virtual ~Thread();

    /** 
     * Start the thread. 
     * @return true on success, false otherwise 
     */
    Return Run();
    /** 
     * Start the thread. 
     *	return true on success, false otherwise 
     */
    Return Run(const Attributes& a);
    /** 
     * Users should override this to define the thread function to be
     * executed. 
     * @return Pointer to something, optionally NULL. 
     */
    virtual void* operator()() = 0;
    /** 
     * Stop this thread. 
     * @param ret Address of possible return value 
     */ 
    void Stop(void* ret=0);
    /** 
     * Detach this thread 
     * @return true on success, false otherwise 
     */ 
    Return Detach();
    
    /** 
     * Cancel this thread.
     * @return true on success, false otherwise 
     */
    Return Cancel();
    /** 
     * Test for cancellation of this thread 
     */
    void TestCancel();
    /** 
     * Enable/Disable cancellation of this thread. This must be
     * executed in the corresponding thread (i.e., in the function
     * operator, or member functions called from there) to have any
     * effect. 
     * @param enable If true, enable cancellation, otherwise disable
     * cancellation. 
     * @return true on success, false otherwise 
     */
    Return EnableCancel(bool enable=true);
    /** 
     * Set the cancellation type. This must be executed in the
     * corresponding thread (i.e., in the function operator, or
     * member functions called from there) to have any effect. 
     * @param async If true, cancel as soon as possible, otherwise
     * wait for an (explicit) cancellation point. 
     * @return true on success, false otherwise 
     */
    Return AsyncCancel(bool async=true);

    /** 
     * Join calling thread to this thread.  This must not be executed
     * in the associated thread, or it will have no effect (false is
     * returned). 
     * @param ret Reference to pointer where to store the return
     * value of this thread. 
     * @return true on success, false other wise 
     */ 
    Return Join(void*& ret);
    /** 
     * Like the member function above, except that there's no return
     * parameter. 
     * @return true on success, false other wise 
     */ 
    Return Join();
	
    /** 
     * Test if the thread of this object is the current thread 
     * @return true if we're in our thread. 
     */
    bool IsCurrent() const;
    /** 
     * Check if this thread is running 
     * @return true if the thread is running. 
     */
    bool IsRunning() const { return _is_running; }
    
    /** 
     * Comparison operator 
     */ 
    bool operator==(const Thread& other) const;
    /** 
     * Convert a return value to a string
     * 
     * @param r Return value 
     * 
     * @return String representing the return value
     */
    static const char* Return2String(const Return& r);
    /** 
     * Get the maximum number of threads available on the system
     * 
     * @return maximum number of threads available on the system
     */    
    static unsigned int MaxThreads();
  protected: 
    /** 
     * Clean up handler 
     */
    virtual void HandleCleanup();
    /** 
     * Handle return values 
     */
    static Return HandleReturn(int ret);
    /** 
     * Friend function 
     */ 
    friend void* fee_thread_launcher(void* arg) throw ();
    /** 
     * Friend function 
     */
    friend void fee_thread_cleanup(void* arg);
    /** 
     * Manager is friend 
     */
    friend class ThreadManager;

    /** Name of the thread */
    std::string _name;
    /** Thread object */
    pthread_t      _thread;
    /** Whether we're running or not. */ 
    bool _is_running;
  };


  //__________________________________________________________________
  /** 
   * @class ThreadManager fee_thread.hh <feeserverxx/fee_thread.hh> 
   * @brief Thread manager class.  This is a singleton object that
   * registers all threads started. 
   * @ingroup threads 
   */
  class ThreadManager 
  {
  public:
    /** 
     * Destructor 
     */
    ~ThreadManager();
    /** 
     * Get singleton 
     */
    static ThreadManager& Instance() { return *_instance; }
    /** 
     * Get name of current thread 
     */
    const std::string& Current() const;
    /** 
     * Get the number of registered threads 
     */
    unsigned int NThreads() const { return _map.size(); }
    /** 
     * List all threads 
     */
    void List() const;
  private: 
    /** Thread class is a friend */
    friend class Thread;
    /** trampoline is a friend */ 
    friend void* fee_thread_launcher(void* arg) throw ();
    /** Register a thread */
    bool  Register(const std::string& name, Thread& thread);
    /** De register a thread */
    void Deregister(Thread& thread);
    /** De register a thread */
    void Deregister(const std::string& name);
    /** Ctor */
    ThreadManager();
    /** Ctor */
    ThreadManager(const ThreadManager&) : _map(), _mutex() {}
    /** Type of map */
    typedef std::map<std::string, Thread*> ThreadMap;
    /** The map */
    ThreadMap _map;
    /** Lock */
    Mutex _mutex;
    /** Singleton object */
    static ThreadManager* _instance;
  };
}

#endif
