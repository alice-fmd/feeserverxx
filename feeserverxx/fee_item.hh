#ifndef FEE_ITEM_HH
#define FEE_ITEM_HH
#include <feeserverxx/fee_serv.hh>

// Forward declaration 
class DimService;

namespace FeeServer 
{

  //_________________________________________________________________
  /** @class Item fee_item.hh <feeserverxx/fee_item.hh> 
      @brief Service item.  This class ties a ServiceBase object to a
      DIM service object. This is a class used only internally to the
      fee server core .
      @ingroup services
  */ 
  class Item 
  {
  public:
    /** Constructor 
	@param server Server name 
	@param service Underlying service */
    Item(const std::string& server, ServiceBase& service);
    /** Destructor */
    virtual ~Item();
    /** @return Name of the service */
    const std::string& Name() const;
    /** Update the DIM service if the underlying service is changed,
	or if the @a force flag is @c true. 
	@return true in case of success, false otherwise */
    bool Update(bool force=false);
    /** Set the dead-band */
    void SetDeadband(float v);
    /** Get the dead-band */ 
    float GetDeadband() const;
  protected:
    /** Not implemented */ 
    Item(const Item&);
    /** Not implemented */ 
    Item& operator=(const Item&);
    /** Reference to service */
    ServiceBase& _service;
    /** Our DIM service */
    DimService*  _dim_service;
  };
}

#endif
//
// EOF
//
  
