#ifndef FEE_DBG_HH
#define FEE_DBG_HH
// #include <sstream>
// #define LOG_LOCAL

/** @defgroup rcuce_msg Preprocessor macros to handle messages */
# ifndef LOG_LOCAL
/** @def DO_MSG1 
    @brief Basic macro to forward calls. 
    @param MAIN The main object
    @param LEVEL The message level
    @ingroup rcuce_msg */
#  define DO_MSG1(MAIN,LEVEL) \
     do { FeeServer::Message::Event _lvl_ = FeeServer::Message::LEVEL; \
          MAIN.Msg 
/** @def DO_MSG2
    @brief Encodes a printf-like argument list
    @param args printf like arguments. 
    @ingroup rcuce_msg */
#  if defined(__GNUC__) && defined(__STRICT_ANSI__) 
#   define DO_MSG2(...) (_lvl_, DO_MSG3, __VA_ARGS__); } while (false)
#  else
#   define DO_MSG2(args...) (_lvl_, DO_MSG3, ##args); } while (false)
#  endif
/** @def DO_MSG3
    @brief Expands to the function name.
    @ingroup rcuce_msg */
#  define DO_MSG3 __PRETTY_FUNCTION__ 
/** @def DO_MSG4
    @brief Expands to noting
    @ingroup rcuce_msg */
#  define DO_MSG4 
# else
#  define DO_MSG1(MAIN,LEVEL) \
     do { printf(#LEVEL ":\t%s line %d: ", __FILE__, __LINE__);
#  define DO_MSG2 printf
#  define DO_MSG3 
#  define DO_MSG4 ; printf("\n"); fflush(stdout); } while (false) 
# endif

// Debug stuff.
# ifdef __DEBUG
/** Macro for debug output. 
    @param M Main object 
    @param X printf-like Arguments to message 
    @ingroup rcuce_msg */
#  define DEBUG(M,X) DO_MSG1(M,Debug) DO_MSG2 X DO_MSG4
# else 
#  define DEBUG(M,X) do { } while(false)
# endif

/** Macro for information output 
    @param M Main object 
    @param X printf-like Arguments to message 
    @ingroup rcuce_msg */
# define INFO(M,X) DO_MSG1(M,Info) DO_MSG2 X DO_MSG4
/** Macro for warning output 
    @param M Main object 
    @param X printf-like Arguments to message 
    @ingroup rcuce_msg */
# define WARN(M,X) DO_MSG1(M,Warning) DO_MSG2 X DO_MSG4
/** Macro for warning output 
    @param M Main object 
    @param X printf-like Arguments to message 
    @ingroup rcuce_msg */
# define ERROR(M,X) DO_MSG1(M,Error) DO_MSG2 X DO_MSG4

# ifdef __DEBUG
#  include <string>
namespace FeeServer
{
  /** @class DebugGuard rcuce_msg.hh <rcuce/rcuce_msg.hh>
      @brief A debug guard to trace calls 
      @ingroup rcuce_msg */
  struct DebugGuard
  {
    /** Current indention level */
    static int  _level;
    /** Static instance of debug flag */
    static unsigned int _debug;
    /** The message to print */
    std::string _message;
    /** Make a debug guard.  Prints the enter message
	@param fmt Format of the message. */
    DebugGuard(unsigned int lvl, 
	       const char*  func, 
	       const char*  file, 
	       unsigned int line,
	       const char*  fmt, ...);
    /** Destroy a debug guard.  Prints the exit message */
    ~DebugGuard();
    /** Static member function to print a message 
	@param fmt Format of the message */
    static void Message(unsigned int lvl, 
			const char*  func, 
			const char*  file, 
			unsigned int line,
			const char*  fmt, ...);
  };
}

/** @ingroup rcuce_msg
    Enable/Disable tracing 
    @param X if @c true, enable tracing */
#  define DTRACE(X) do { FeeServer::DebugGuard::_debug = X; } while(false)
#define  DLEVEL (FeeServer::DebugGuard::_debug)
#define  DEXEC(L,X) do { if (FeeServer::DebugGuard::_debug >= L) { \
      X } } while (false)

/** @ingroup rcuce_msg
    Make a debug guard. 
    @param X a unique (to the scope) identifier 
    @param args @c printf -like arguments */
#  if defined(__GNUC__) && defined(__STRICT_ANSI__) 
#   define DGUARD(L,...) \
  FeeServer::DebugGuard _g_ ## __LINE__ \
  (L, __PRETTY_FUNCTION__, __FILE__, __LINE__, __VA_ARGS__ )
#   else
#   define DGUARD(L,args...) \
  FeeServer::DebugGuard _g_ ## __LINE__ \
  (L, __PRETTY_FUNCTION__, __FILE__, __LINE__, args )
#endif
/** @ingroup rcuce_msg
    Make a trace output in the middle of a FeeServer::DebugGuard lifetime
    @param args @c printf -like arguments */
#  if defined(__GNUC__) && defined(__STRICT_ANSI__) 
#   define DMSG(L, ...)     \
  FeeServer::DebugGuard::Message(L, __PRETTY_FUNCTION__, \
				 __FILE__, __LINE__, __VA_ARGS__ )
#  else
#   define DMSG(L, args...)     \
  FeeServer::DebugGuard::Message(L, __PRETTY_FUNCTION__, \
				 __FILE__, __LINE__, args )
#  endif

# else // __DEBUG
#  define DTRACE(X)         do {} while(false)
#  define DLEVEL            0
#  define DEXEC(L,X)        do {} while(false)
#  define DINIT(X)          do {} while(false)
#  define DGUARD(L,args...) do {} while(false)
#  define DMSG(L,args..)    do {} while(false)
# endif // __DEBUG
#endif // FEE_DBG_HH

//
// EOF
//
