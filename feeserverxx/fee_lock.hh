#ifndef FEE_LOCK_HH
#define FEE_LOCK_HH

#include <pthread.h>
#include <string>
// #include <stdexcept>
namespace std
{
  struct exception;
}
namespace FeeServer 
{
  // Forward declaration 
  class Condition;

  //__________________________________________________________________
  /** @class Mutex fee_lock.hh <feeserverxx/fee_lock.hh>
      @brief Mutex lock 
      @ingroup threads 
  */
  class Mutex 
  {
  public:
    /** Attributes of mutexes */ 
    class Attributes 
    {
    public: 
      /** types of mutex'es */
      enum Kind { 
	/** Normal mutex */
	Fast, 
	/** Recursive locking */
	Recursive, 
	/** Error checking */
	Checking
      };
      /** Constructor */ 
      Attributes(Kind type=Fast); 
      /** Destructor */
      virtual ~Attributes();
      /** Set the kind of mutex */
      bool Set(Kind type=Fast);
      /** Get the kind of mutex */
      Kind Type() const;
    protected: 
      friend class Mutex;
      pthread_mutexattr_t _attr;
    };
	
    /** Return codes */ 
    enum Return {
      Success, 
      BadMutex, 
      DeadLock, 
      Busy, 
      Denied 
    };

    /** Constructor 
	@param name Name of the mutex */
    Mutex(const std::string& name="");
    /** Constructor 
	@param name Name of the mutex 
	@param attr Attributes */
    Mutex(const std::string& name, const Attributes& attr);
    /** Destructor */
    virtual ~Mutex();
    /** Lock the mutex.  Can dead-lock calling thread if type is Fast,
	and the thread has already locked the mutex.  If type is
	recursive, one must unlock the mutex as many times as it has
	been locked by a thread.  The error checking will fail. 
	@return false if the lock failed */
    Return Lock();
    /** Lock the mutex. 
	@return false if the lock failed */
    Return TryLock();
    /** Unlock the mutex. 
	@return false if the lock failed */
    Return Unlock();
    /** Get the name of the mutex */
    const std::string& Name() const { return _name; }
    /** 
     * Get string representing the return value
     * 
     * @param r Return value
     * 
     * @return String representing the return value
     */
    static const char* Return2String(const Return& r);
  protected: 
    /** Translate return values to our enum */ 
    Return HandleReturn(const char* oper, int ret) const;
    /** Conditions are firends */ 
    friend class Condition;
    /** Mutex */
    pthread_mutex_t _mutex;
    /** Name of mutex */
    std::string _name;
  };

  //__________________________________________________________________
  /** @class Guard fee_lock.hh <feeserverxx/fee_lock.hh> 
      @brief Class to lock a mutex in a scope 
      @ingroup threads 
  */
  class Guard 
  {
  public: 
    /** Constructor. Locks the mutex passed */
    Guard(Mutex& mutex) throw (std::exception);
    /** Destructor. Unlocks the mutex passed */
    ~Guard();
  protected: 
    /** Mutex */
    Mutex& _mutex;
  };
}

#endif
//
// EOF
//
