#include <feeserverxx/fee_issue.hh>
#include <feeserverxx/fee_main.hh>
#include <feeserverxx/fee_msg.hh>
#include <feeserverxx/fee_packet.hh>
#include <feeserverxx/fee_ce.hh>
#include <iostream>
#if 1
#define MSG(X) _main.Msg X
#else
#define MSG(X) DO_MSG X
#define DO_MSG(X,Y,Z, args...) \
  do { printf(Y); printf(" "); printf (Z, ##args); printf("\n") ; \
       fflush(stdout); } while(false)
#endif

//====================================================================
void*
FeeServer::IssueThread::operator()() 
{
  Thread::Return ret = Thread::Success;
  // Enable cancels 
  if ((ret = EnableCancel(true)) != Success) 
    MSG((Message::Warning, "Issue", 
	 "Unable to configure issue thread for cancellation %d", ret));

  // Cancel immediately
  if ((ret = AsyncCancel(true)) != Success) 
    MSG((Message::Warning, "Issue", 
	 "Unable to configure issue thread for async cancellation: %d", ret));

  if (!_command) {
    MSG((Message::Warning, "Issue", "no command set for issue thread"));
    return 0;
  }
  if (!_acknowledge) {
    MSG((Message::Warning, "Issue","no acknowledge set for issue thread"));
    return 0;
  }


  // Forward the message to the CE here 
  // _main.Msg(Message::Warning, "Issue", "sending issue to CE");
  _acknowledge->_header._error_code = 
    _main.CtrlEngine().Issue(_command->_data,     _command->_size, 
			     _acknowledge->_data, _acknowledge->_size);
  MSG((Message::Debug, "Issue", "CE returned %d", 
       _acknowledge->_header._error_code));

  // Reset pointers to zero. 
  // _command = 0;
  // _acknowledge = 0;

  // Defer cancels 
  if ((ret = AsyncCancel(false)) != Success) 
    MSG((Message::Warning, "Issue", 
	 "Unable to configure issue thread for async cancellation: %d", ret));

  // Lock wait mutex 
  Mutex::Return mret = Mutex::Success;
  if ((mret = _main._wait_mutex.Lock()) != Mutex::Success) 
    MSG((Message::Warning, "Issue",
	 "Unable to lock mutex in issue thread %d", mret));
      
  // Broad cast that we're done 
  _main._wait_condition.Broadcast();

  // And now unlock the mutex 
  if ((mret = _main._wait_mutex.Unlock()) != Mutex::Success) 
    MSG((Message::Warning, "Issue", 
	 "Unable to release mutex in issue thread: %d", mret));

  MSG((Message::Debug, "Issue", "returning to the caller"));
  return 0;
}

//
// EOF
//
