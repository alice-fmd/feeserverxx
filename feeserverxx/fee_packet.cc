#include <feeserverxx/fee_packet.hh>
#include <feeserverxx/fee_sum.hh>
#include <iostream>
#include <iomanip>
#include <cstring>

//====================================================================
FeeServer::Message::Message()
  : _event_type(Info)
{
  Clear();
}

//____________________________________________________________________
size_t
FeeServer::Message::Size()
{
  static size_t ret = (sizeof(int) 
		       + DetectorSize 
		       + SourceSize
		       + DescriptionSize
		       + DateSize);
  return ret;
}

//____________________________________________________________________
void
FeeServer::Message::Clear()
{
  _event_type = 0;
  for (size_t i = 0; i < DetectorSize;    i++) _detector[i]    = '\0';
  for (size_t i = 0; i < SourceSize;      i++) _source[i]      = '\0';
  for (size_t i = 0; i < DescriptionSize; i++) _description[i] = '\0';
  for (size_t i = 0; i < DateSize;        i++) _date[i]        = '\0';
}

//____________________________________________________________________
bool
FeeServer::Message::FromBuffer(byte_ptr buf, size_t size)
{
  size_t int_size = sizeof(int);
  if (!buf) return false;
  if (size != Size())  return false;
  size_t off = 0;
  size_t n   = int_size;
  memcpy(&_event_type, &(buf[off]), n); off += n; n=DetectorSize;
  memcpy(_detector,    &(buf[off]), n); off += n; n=SourceSize;
  memcpy(_source,      &(buf[off]), n); off += n; n=DescriptionSize;
  memcpy(_description, &(buf[off]), n); off += n; n=DateSize;
  memcpy(_date,        &(buf[off]), n); off += n;
  return size == off;
}

//____________________________________________________________________
bool
FeeServer::Message::ToBuffer(byte_array& buf)
{
  size_t int_size = sizeof(int);
  if (buf.size() != Size()) buf.resize(Size());
  size_t off = 0;
  size_t n   = int_size;
  memcpy(&(buf[off]), &_event_type, n); off += n; n=DetectorSize;
  memcpy(&(buf[off]), _detector,    n); off += n; n=SourceSize;
  memcpy(&(buf[off]), _source,      n); off += n; n=DescriptionSize;
  memcpy(&(buf[off]), _description, n); off += n; n=DateSize;
  memcpy(&(buf[off]), _date,        n); off += n;
  return Size() == off;
}

//____________________________________________________________________
const char*
FeeServer::Message::Code2String(int e) 
{
  switch (e) {
  case Message::Info:         return "Info";    break;
  case Message::Warning:      return "Warning"; break;
  case Message::Error:        return "Error";   break;
  case Message::AuditFailure: return "Failure"; break;
  case Message::AuditSuccess: return "Success"; break;
  case Message::Debug:        return "Debug";   break;
  case Message::Alarm:        return "Alarm";   break;
  }
  return "Unknown"; 
}

//____________________________________________________________________
std::ostream& 
FeeServer::operator<<(std::ostream& o, const Message& m)
{
  o << "Message:\n"
    << "\tEvent code: " << Message::Code2String(m._event_type) << "\n"
    << "\tDetector:  " << m._detector << "\n"
    << "\tSource:    " << m._source   << "\n"
    << "\tDate:      " << m._date     << "\n"
    << "\t" << m._description;
  return o;
}

//====================================================================
FeeServer::Header::Header(unsigned int id, short error, 
			  unsigned short flags)
  : _id(id), 
    _error_code(error), 
    _flags(flags), 
    _check_sum(0)
{}

//____________________________________________________________________
FeeServer::Header::Header(const Header& other)
  : _id(other._id), 
    _error_code(other._error_code), 
    _flags(other._flags), 
    _check_sum(other._check_sum)
{}

//____________________________________________________________________
size_t
FeeServer::Header::Size()
{
  static size_t ret = (sizeof(unsigned int) 
		       + sizeof(short)
		       + sizeof(unsigned short)
		       + sizeof(unsigned int));
  return ret;
}

//____________________________________________________________________
size_t
FeeServer::Header::ToBuffer(byte_array& buf) const 
{
  if (buf.size() < sizeof(Header)) buf.resize(Size());
  size_t off = 0;
  size_t n   = sizeof(_id);
  memcpy(&(buf[off]), &_id, n);         off += n; n = sizeof(_error_code);
  memcpy(&(buf[off]), &_error_code, n); off += n; n = sizeof(_flags);
  memcpy(&(buf[off]), &_flags,      n); off += n; n = sizeof(_check_sum);
  memcpy(&(buf[off]), &_check_sum,  n); off += n;
  return off;
}

//____________________________________________________________________
size_t
FeeServer::Header::FromBuffer(const byte_array& buf)
{
  return FromBuffer(const_cast<const byte_ptr>(&(buf[0])), buf.size());
}
  

//____________________________________________________________________
size_t
FeeServer::Header::FromBuffer(const byte_ptr buf, size_t size)
{
  if (size < Size()) return 0;
  if (!buf) return 0;
  size_t off = 0;
  size_t n   = sizeof(_id);
  memcpy(&_id,         &(buf[off]), n); off += n; n = sizeof(_error_code);
  memcpy(&_error_code, &(buf[off]), n); off += n; n = sizeof(_flags);
  memcpy(&_flags,      &(buf[off]), n); off += n; n = sizeof(_check_sum);
  memcpy(&_check_sum,  &(buf[off]), n); off += n;
  return off;
}
  

//____________________________________________________________________
bool 
FeeServer::Header::ValidateChecksum(const byte_ptr buf, size_t size) const
{
  if (!(_flags & HasChecksum)) return true;
  return _check_sum == CheckSum(buf, size);
}

//____________________________________________________________________
void 
FeeServer::Header::SetChecksum(const byte_ptr buf, size_t size) 
{
  _check_sum = (_flags & HasChecksum ? CheckSum(buf, size) : 0);
}

//____________________________________________________________________
FeeServer::Header&
FeeServer::Header::operator=(const Header& other) 
{
  _id         = other._id;
  _error_code = other._error_code;
  _flags      = other._flags;
  _check_sum  = other._check_sum;
  return *this;
}

//____________________________________________________________________
const char*
FeeServer::Header::Code2String(short e) 
{
  if (e > 0) return "information";
  if (e < -IclBase) return "InterCom Layer";
  if (e < -DetectorBase) return "Detector";
  switch (-e) {
  case	Ok:             return "Success"; 			break;
  case	TimeOut:        return "Timeout"; 			break;
  case	NullPointer:    return "Nullpointer encountered"; 	break;
  case	InvalidParam:   return "Invalid parameter"; 		break;
  case	WrongState:     return "Not valid for state"; 		break;
  case	Failed:         return "Failed"; 			break;
  case	InitFailed:     return "Initialisation failed"; 	break;
  case	ChecksumFailed: return "Wrong checksum"; 		break;
  case	MonitorFailed:  return "Monitoring initialising failed";break;
  case	OutOfMemory:    return "Insufficient memory"; 		break;
  case	ThreadError:    return "Thread error"; 			break;
  case	ItemExists:     return "Already published"; 		break;
  case	Unknown:        
  default:		
    break;
  }
  return "Unexpected return value"; 	
}
    
//____________________________________________________________________
std::ostream& 
FeeServer::operator<<(std::ostream& o, const Header& h)
{
  o << "Header:\n"
    << "\tId:            " << h._id << "\n"
    << "\tError code:    0x" << std::hex << h._error_code << " (" 
    << Header::Code2String(h._error_code) << ")\n" 
    << "\tFlags:         0x" << h._flags << "\n"
    << "\tCheck sum:     0x" << h._check_sum << std::dec;
  return o;
}


//====================================================================
FeeServer::Packet::Packet(unsigned int id, 
			  short error, 
			  unsigned short flags, 
			  byte_ptr data, 
			  size_t size) 
  : _header(id, error, flags), 
    _size(size), 
    _data(0)
{
  if (_size <= 0) return;
  _data.resize(size);
  std::copy(&(data[0]), &(data[size]), &(_data[0]));
  _header.SetChecksum(&(_data[0]), _size);
}

//____________________________________________________________________
FeeServer::Packet::Packet(const Header& header, 
			  byte_ptr data, 
			  size_t size) 
  : _header(header), 
    _size(size),
    _data(0)
{
  if (size <= 0) return;
  _data.resize(size);
  std::copy(&(data[0]), &(data[size]), &(_data[0]));
  _header.SetChecksum(&(_data[0]), _size);
}

//____________________________________________________________________
size_t
FeeServer::Packet::ToBuffer(byte_array& buf) const
{
  if (buf.size() < _size + Header::Size()) 
    buf.resize(_size + sizeof(_header));
  size_t off = _header.ToBuffer(buf);
  std::copy(&(_data[0]), &(_data[_size]), &(buf[off]));
  return off+_size;
}

//____________________________________________________________________
size_t
FeeServer::Packet::FromBuffer(const byte_ptr buf, size_t size)
{
  size_t off = 0;
  _size      = 0;
  if ((off = _header.FromBuffer(buf, size)) != Header::Size()) return 0;
  _size = size - off;
  if (_size > 0) {
    if (_data.size() <_size) _data.resize(_size);
    std::copy(&(buf[off]), &(buf[off+_size]), &(_data[0]));
  }
  return off+size;
}

//____________________________________________________________________
bool
FeeServer::Packet::ValidateChecksum() const
{
  return _header.ValidateChecksum(const_cast<const byte_ptr>(&(_data[0])), 
				  _size);
}

//____________________________________________________________________
std::ostream& 
FeeServer::operator<<(std::ostream& o, const Packet& p)
{
  o << "Packet " << p._header << "\n"
    << "Packet data:\n"
    << "\tSize:          " << p._size << "\n"
    << "\t" << std::hex << std::setfill('0');
  for (size_t i = 0; i < p._size; i++) {
    if (i % 10 == 0 && i != 0) o << "\n\t";
    o << " " << std::setw(2) << (unsigned(p._data[i]) & 0xff);
  }
  o << std::dec << std::setfill(' ');
  if (p._size % 10 == 0) o << "\n";
  return o;
}
			   

//____________________________________________________________________
//
// EOF
//
