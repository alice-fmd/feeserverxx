#include <feeserverxx/fee_ack.hh>
#include <feeserverxx/fee_packet.hh>
#include <feeserverxx/fee_dbg.hh>
#include <dim/dis.hxx>
#include <iostream>
#include <iomanip>
#include <sstream>

//____________________________________________________________________
FeeServer::AcknowledgeChannel::AcknowledgeChannel(const std::string& server)
  : _service(0),
    _server(server), 
    _buffer(256)
{
  std::stringstream s;
  s << server << "_Acknowledge";
  char format[2] = { 'C', '\0' };
  _service = new DimService(const_cast<char*>(s.str().c_str()), format, 0, 0); 
  Header h(0, 0, 0);
  size_t size = h.ToBuffer(_buffer);
  _service->updateService( &(_buffer[0]), size);
}

//____________________________________________________________________
FeeServer::AcknowledgeChannel::~AcknowledgeChannel()
{
  if (_service) delete _service;
}

//____________________________________________________________________
void
FeeServer::AcknowledgeChannel::Send(Acknowledge& ack)
{
  size_t size = ack.ToBuffer(_buffer);
  DEXEC(5, std::cout << "Sending acknowledge:\n"
	             << ack << std::endl;);
  _service->updateService(&(_buffer[0]), size);
  DEXEC(5, std::cout << "Send bytes:\n\t"
	<< std::hex << std::setfill('0');
	for (size_t i = 0; i < size; i++) { 
	  if (i % 10 == 9) std::cout << "\n\t";
	  std::cout << " 0x" << std::setw(2) << short(_buffer[i]);
	}
	std::cout << std::endl;
	);
}
  
  
//____________________________________________________________________
//
// EOF
//
