#include <feeserverxx/fee_main.hh>
#include <feeserverxx/fee_msg.hh>
#include <feeserverxx/fee_ack.hh>
#include <feeserverxx/fee_cmd.hh>
#include <feeserverxx/fee_scmd.hh>
#include <feeserverxx/fee_packet.hh>
#include <feeserverxx/fee_ce.hh>
#include <feeserverxx/fee_sum.hh>
#include <dim/dis.hxx>
#include <stdexcept>
#include <cstdarg>
#include <iostream>
#include <sstream>
#include <csignal>
#include <cstdio>

namespace { 
  volatile sig_atomic_t _stop = 0;
}

namespace FeeServer 
{
  /** Handler of DIM errors and exit */
  class ErrorExitHandler : public DimErrorHandler, public DimExitHandler
  {
  public:
    /** Constructor 
	@param m Reference to FeeServer main */
    ErrorExitHandler(Main& m) : _main(m) 
    {
      DimServer::addErrorHandler(this);
      DimServer::addExitHandler(this);
    }
  private:
    /** Reference to main object */ 
    Main& _main;
    /** Handle DIM errors */ 
    virtual void errorHandler(int severity, int code, char *msg)
    {
      _main.ErrorHandler(severity, code, msg);
    }
    /** Handle DIM exit requests */
    virtual void exitHandler(int code)
    {
      _main.ExitHandler(code);
    }
  };
}

//____________________________________________________________________
void
interrupt_handler(int sig) 
{
  // Maybe Main needs to be a singleton for this to work!?
  std::cout << "Recieved signal " << strsignal(sig) 
	    << " - will stop FeeServer" << std::endl;
  _stop = sig;
  exit(1);
}

//====================================================================
struct CommandException : std::exception
{
  /** construct a command exception. 
      @param id Request id 
      @param event Event type 
      @param error Return code 
      @param format @c printf-like format */
  CommandException(unsigned int id, FeeServer::Message::Event event, 
		   short error, const char* format, ...) 
    : _id(id), 
      _error(error),
      _event(event), 
      _what()
  {
    va_list ap;
    va_start(ap, format);
    char buf[1024];
    vsnprintf(buf, 1024, format, ap);
    _what = buf;
    va_end(ap);
  }
  virtual ~CommandException() throw () {}
  const char* what() const throw () { return _what.c_str(); }
  /** Request Id */
  unsigned int _id;
  /** return value */
  short _error;
  /** Event type */
  FeeServer::Message::Event _event;
  /** Message */
  std::string _what;
};


//====================================================================
template <typename T>
bool env2val(T& v, const std::string& env)
{
  if (env.empty()) return false;
  const char* val = getenv(env.c_str());
  if (!val) return false;
  
  std::stringstream s(val);
  s >> v;
  return true;
}

//====================================================================
FeeServer::Main::Main(const std::string& name, 
		      const std::string& detector,
		      const std::string& dns) 
  : _error_exit(0),
    _control_engine(0),
    _name(name), 
    _detector(detector), 
    _retval(0),
    _state(Collecting),
    _msg_channel(0), 
    _ack_channel(0), 
    _cmd_channel(0), 
    _msg_watchdog(0),
    _ack(0,0,0,0,0), 
    _command_mutex("command_mutex"), 
    _wait_mutex("wait_mutex"),
    _wait_condition(),
    _detached_attr(true),
    // _detached_attr(false),
    _issue_thread(*this), 
    _issue_timeout(60000),
    _mon_thread(*this), 
    _commands(),
    _msg_mask(Message::Info | 
	      Message::Warning | 
	      Message::Error | 
	      Message::AuditFailure | 
	      Message::AuditSuccess |
	      Message::Alarm), 
    _msg_timeout(10000)
{
  _stop     = 0;
  _instance = this;
  InstallHandlers();
  std::string tdns = dns;
  if (tdns.empty() && getenv("DIM_DNS_NODE")) tdns = getenv("DIM_DNS_NODE");
  if (!tdns.empty()) DimServer::setDnsNode(tdns.c_str());
  if (_name.empty() && getenv("FEE_SERVER_NAME")) 
    _name = getenv("FEE_SERVER_NAME");
  if (_detector.empty() && getenv("FEE_SERVER_DETECTOR")) 
    _detector = getenv("FEE_SERVER_DETECTOR");
  unsigned int log_timeout;
  if (env2val(log_timeout, "FEE_LOGWATHDOG_TIMEOUT")) 
    _msg_timeout = log_timeout;
  unsigned int log_mask;
  if (env2val(log_mask, "FEE_LOG_LEVEL")) _msg_mask = log_mask;

  _error_exit = new ErrorExitHandler(*this);
  std::cout << "\n"
	    << "  ** FeeServer version: " << VERSION << " **\n\n"
	    << "FeeServer name:     " << _name << "\n"
	    << "Using DIM_DNS_NODE: " << tdns << "\n"
	    << std::endl;

}

//____________________________________________________________________
FeeServer::Main* FeeServer::Main::_instance = 0;

//____________________________________________________________________
FeeServer::Main::~Main() 
{
  CleanUp();
}

//____________________________________________________________________
const std::string&
FeeServer::Main::Version()
{
  static std::string version = PACKAGE_VERSION;
  return version;
}

//____________________________________________________________________
const std::string&
FeeServer::Main::Package()
{
  static std::string package = PACKAGE_NAME;
  return package;
}

//____________________________________________________________________
FeeServer::Main& 
FeeServer::Main::Instance() 
{
  if (!_instance) throw std::runtime_error("No FeeServer::Main defined");
  return *_instance;
}

//____________________________________________________________________
const char*
FeeServer::Main::VersionString() const
{
  return PACKAGE_VERSION;
}
//____________________________________________________________________
const char*
FeeServer::Main::PackageString() const
{
  return PACKAGE_NAME;
}

//____________________________________________________________________
int
FeeServer::Main::Run(const unsigned int ce_timeout)
{
  if (_name.empty()) {
    std::cerr << "FeeServer name not defined!" << std::endl;
    return 1;
  }
  if (_detector.empty()) {
    std::cerr << "Detector name not defined!" << std::endl;
    return 1;
  }
  if (!_control_engine) {
    std::cerr << "No control engine defined!" << std::endl;
    return 1;
  }
  // Initialize
  Init(ce_timeout);

  dtq_sleep(1);
  ThreadManager::Instance().List();

  try { 
    // Loop forever 
    while (true) { 
      // pause();
      // Msg(Message::Info, "Main", "Returned from pause");
      // dtq_sleep(1);
      sleep(1);
      if (_stop) Stop(0);
    }
  }
  catch (std::exception& e) { 
    Msg(Message::Error, "Main", 
	"Unknown exception: %s", e.what());
  }
  
  
  return _retval;
}

//____________________________________________________________________
void
FeeServer::Main::Init(const unsigned int timeout)
{
  try { 
    int e = 0;
    Thread::Return thr_ret = Thread::Success;
    { // Scope for mutex
      Guard g(_control_engine->_mutex);
      
      // Create init thread to start CE
      // Thread::Return thr_ret = _control_engine->Run(_detached_attr);
      thr_ret = _control_engine->Run();
      if (thr_ret != Thread::Success) {
	std::cerr << "Thread creation failed!" << std::endl;
	throw Header::InitFailed;
      }
      
      // Wait for CE to finish set-up. 
      Condition::Return cond_ret = 
	_control_engine->_condition.Wait(_control_engine->_mutex, timeout);
      if (cond_ret == Condition::BadTime) {
	// Do a primitive sleep instead 
	// Init thread locks this mutex 
	_control_engine->_mutex.Unlock();
	usleep(timeout * 1000);
	dtq_sleep(timeout % 1000);
	if (!_control_engine->_is_ready) cond_ret = Condition::TimeOut;
	_control_engine->_mutex.Lock();
      }

      // If we timed out, or similar, then flag this as an error 
      if (cond_ret != Condition::Success) {
	thr_ret = _control_engine->Cancel();
	std::cerr << "Control engine didn't start (" << cond_ret << ")" 
		  << std::endl;
	if (thr_ret != Thread::Success) 
	  std::cerr << "No thread to cancel" << std::endl;
	throw e = Header::InitFailed;
      }
    }

    // Create the core services 
    _msg_channel = new MessageChannel(_name, _detector, _msg_mask);
    _ack_channel = new AcknowledgeChannel(_name);
    _cmd_channel = new CommandChannel(_name, *this);

    // Start the server 
    DimServer::start(_name.c_str());
    
    // Set state to running
    _state = Running;

    // Start the monitoring thread 
    // thr_ret = _mon_thread.Run(_detached_attr);
    thr_ret = _mon_thread.Run();
    if (thr_ret  != Thread::Success) {
      Msg(Message::Error, "Main", 
	  "monitoring thread failed to start");
      std::cout << "Monitor failed!" << std::endl;
      throw e = Header::MonitorFailed;
    }

    // Make and start log watch dog
    _msg_watchdog = new MessageWatchdog(*_msg_channel, _msg_timeout);
    thr_ret       = _msg_watchdog->Run();
    if (thr_ret != Thread::Success) 
      Msg(Message::Warning, "Main", 
	  "cannot start log watch dog thread, continuing");

    // Signal CE that he can continue;
    Guard g(_control_engine->_mutex);
    _control_engine->_can_continue = true;
    _control_engine->_condition.Broadcast();
  } catch (int s) {
    std::cerr << "Init failed, removing services" << std::endl;
    Stop(InitRestart);
  }
  return;
}

//____________________________________________________________________
void
FeeServer::Main::CleanUp()
{
  // Tell control engine to clean up 
  if (_control_engine) _control_engine->CleanUp();

  // Thread::Return thr_ret = Thread::Success;
  // Stop the monitoring thread. 
  if (_mon_thread.IsRunning()) { 
    // _mon_thread.Resume();
    /* thr_ret = */ _mon_thread.Cancel();
  }
  // Stop the init thread - if still running. 
  if (_control_engine && _control_engine->IsRunning()) {
    /* thr_ret =  */ _control_engine->Cancel();
    // delete _control_engine;
  }
  // Stop the log watch dog thread 
  if (_msg_watchdog && _msg_watchdog->IsRunning()) {
    /* thr_ret = */ _msg_watchdog->Cancel();
    // delete _msg_watchdog;
  }

  if (_error_exit) delete _error_exit;
  // Stop serving DIM services. 
  // DimServer::stop();

  // Show running threads
  delete _msg_watchdog;

  if (_msg_channel) delete _msg_channel;
  if (_ack_channel) delete _ack_channel;
  if (_cmd_channel) delete _cmd_channel;

  // Remove all commands 
  RemoveCommands();

  // Remove all services 
  RemoveServices();

  ThreadManager::Instance().List();
}

//____________________________________________________________________
void
FeeServer::Main::Stop(int ret)
{
  Msg(Message::Info, "Main", 
      "Exiting Feeserver (exit state: %d)", ret);
  CleanUp();

  usleep(100);
  exit(ret);
}

//____________________________________________________________________
void
FeeServer::Main::InstallHandlers()
{
  Msg(Message::Debug, "", "Installing signal handlers");
  struct sigaction* action  = new struct sigaction;
  sigset_t   mask;
  sigemptyset(&mask);

  int sigs[] = { SIGFPE, SIGILL, SIGSEGV, SIGBUS, SIGABRT, SIGSYS,
		 SIGTERM, SIGINT, SIGQUIT, SIGKILL, SIGHUP, -1 };
  int* sig = sigs;
  while (*sig != -1) { 
    sigaddset(&mask, *sig);
    sig++;
  }

  action->sa_handler = interrupt_handler;
  action->sa_flags   = 0;
  action->sa_mask    = mask;
  
  sig = sigs;
  while (*sig != -1) { 
    sigaction(*sig, action, NULL);
    sig++;
  }
}


//____________________________________________________________________
void
FeeServer::Main::HandleSignal(int /*sig*/)
{
}

//====================================================================
void
FeeServer::Main::Msg(Message::Event event, const std::string& where, 
		     const char* format, ...)
{
  va_list ap;
  va_start(ap, format); 
  char buf[1024];
  vsnprintf(buf, sizeof(buf)-1, format, ap);
  va_end(ap);
  
  if (_msg_channel) {
    _msg_channel->Send(event, where, buf);
    return;
  }
  
  if (!(event & _msg_mask)) return;
  std::cout << Message::Code2String(event) << "\t[";
  time_t     epoch_now = time(NULL);
  struct tm* local_now = localtime(&epoch_now);
  char dbuf[20];
  strftime(dbuf, sizeof(buf)-1, "%Y-%m-%d %H:%M:%S",local_now);
  dbuf[19] = '\0';
  std::cout << dbuf << "] from " << _detector << "::" 
	    << _name << (where.empty() ? "" : "/") << where 
	    << ": " << buf << std::endl;
}
  
//____________________________________________________________________
void
FeeServer::Main::SetLogLevel(unsigned int mask) 
{
  _msg_mask = mask;
  if (!_msg_channel) return;
  _msg_channel->SetLogMask(mask);
}
//____________________________________________________________________
unsigned int
FeeServer::Main::GetLogLevel() const
{
  if (!_msg_channel) return _msg_mask;
  return _msg_mask = _msg_channel->LogMask();
}
//____________________________________________________________________
void
FeeServer::Main::SetWatchdogTimeout(unsigned int timeout) 
{
  _msg_timeout = timeout;
  if (!_msg_watchdog) return;
  _msg_watchdog->SetTimeout(_msg_timeout);
}
//____________________________________________________________________
unsigned int
FeeServer::Main::GetWatchdogTimeout() const
{
  if (!_msg_watchdog) return _msg_timeout;
  return _msg_timeout = _msg_watchdog->Timeout();
}


//====================================================================
bool
FeeServer::Main::AddCommand(ServerCommand* c) 
{
  if (!c) return false;
  _commands.push_back(c);
  return true;
}

//____________________________________________________________________
void
FeeServer::Main::RemoveCommands()
{
  // Remove all commands 
  for (CommandList::iterator i = _commands.begin(); i != _commands.end();++i)
    if ((*i)) delete (*i);
  _commands.clear();
}

//____________________________________________________________________
void
FeeServer::Main::HandleCommand(Command& cmd)
{
  Guard g(_command_mutex);

  // We can only process commands in the running and error state. 
  // if (_state != Running && _state != Error) return;

  try {
    if (!cmd._header_ok) 
      throw CommandException(cmd._header._id, Message::Warning, 
			     -Header::InvalidParam,"invalid parameter");

    if (!cmd.ValidateChecksum()) 
      throw CommandException(cmd._header._id, Message::Warning, 
			     -Header::ChecksumFailed, 
			     "FeeServer received corrupted command data "
			     "(expected check sum 0x%x but got 0x%x)", 
			     cmd._header._check_sum, 
			     CheckSum(&(cmd._data[0]), cmd._size));

    // The acknowledge structure 
    _ack._header._id         = cmd._header._id;
    _ack._header._flags      = cmd._header._flags;
    _ack._header._check_sum  = 0;
    _ack._header._error_code = Header::Unknown;
    _ack._size               = 0;
    // Loop through known commands and check if they want to handle
    // the request.  
    bool is_handled = false;
    for (CommandList::iterator i=_commands.begin(); i != _commands.end();++i) {
      if (!(*i)) continue;
      if (!(*i)->CanHandle(cmd._header._flags)) continue; 
      _ack._header._error_code = (*i)->Handle(cmd._data, cmd._size, 
					      _ack._data, _ack._size);
      is_handled = true;
      break;
    }
    if (!is_handled) { 
      // Do not process commands for the CE when in error state 
      if (_state == Error) 
	throw CommandException(cmd._header._id, Message::Error,
			       Header::WrongState, 
			       "FeeServer is in error state");

      // Lock our mutex 
      Guard g1(_wait_mutex);

      // Start issue thread 
      Thread::Attributes attr(true);
      _issue_thread.SetCommand(cmd);
      _issue_thread.SetAcknowledge(_ack);
      Thread::Return tret = _issue_thread.Run(attr);
      if (tret != Thread::Success) 
	throw CommandException(cmd._header._id, Message::Error, 
			       -Header::ThreadError, 
			       "couldn't make issue thread: %s", 
			       Thread::Return2String(tret));
      
      // Wait condition 
      Msg(Message::Debug, "", "Waiting for issue thread");
      Condition::Return ret = _wait_condition.Wait(_wait_mutex,_issue_timeout);
      if (ret == Condition::BadTime) {
	// Couldn't set-up the timed wait. Primitive wait instead. 
	Msg(Message::Warning, "", "Couldn't set up watchdog");
	// Release lock (issue thread will lock it). 
	_wait_mutex.Unlock();
	// Primitive sleep 
	usleep(_issue_timeout * 1000);
	// Cancel the thread. 
	ret  = (_issue_thread.Cancel() != Thread::Success ? 
		Condition::TimeOut : Condition::Success);
	_wait_mutex.Lock();
      }

      // Check return from wait. 
      if (ret == Condition::TimeOut) { 
	// Thread timed out. 
	_issue_thread.Cancel();
	throw CommandException(cmd._header._id, Message::Warning, 
			       Header::TimeOut, "timeout on last command");
      }
      else if (ret != Condition::Success) {
	// Some weird error
	_issue_thread.Cancel();
	throw CommandException(cmd._header._id, Message::Warning, 
			       -Header::ThreadError, 
			       "while waiting for issue thread");
      }	
      is_handled = true;
    }
    if (!is_handled || 
	_ack._header._error_code < -Header::Unknown || 
	_ack._header._error_code >= Header::MaxRetval) 
      throw CommandException(cmd._header._id, Message::Warning, 
			     -Header::Unknown, "unknown return value %d/%d",
			     _ack._header._error_code, is_handled);

    _ack._header.SetChecksum(&(_ack._data[0]), _ack._size);
  }
  catch (CommandException& e) {
    Msg(e._event, "Main", e._what.c_str());
    _ack._header._id         = e._id;
    _ack._header._error_code = e._error;
    _ack._header._flags      = 0;
    _ack._header._check_sum  = 0;
    _ack._size               = 0;
  }
  catch (std::exception& e) {
    Msg(Message::Error, "Main", 
	"Unknown exception in command handler %s", e.what());
    _ack._header._id         = cmd._header._id;
    _ack._header._error_code = Header::Unknown;
    _ack._header._flags      = 0;
    _ack._header._check_sum  = 0;
    _ack._size               = 0;
  }
  
  // std::cout << "Sending acknowledge\n" << _ack << std::endl;
  _ack_channel->Send(_ack);
}

//====================================================================
//
// Publish/Unpublish services 
//____________________________________________________________________
struct SuspendGuard 
{
  SuspendGuard(FeeServer::MonitorThread& m) 
    : _mon_thread(m)
  {
    _mon_thread.Suspend();
  }
  ~SuspendGuard() 
  {
    _mon_thread.Resume();
  }
  FeeServer::MonitorThread& _mon_thread;
};

//____________________________________________________________________
int
FeeServer::Main::Publish(ServiceBase& service)
{
  Msg(Message::Debug, "FeeServer::Main::Publish",
      "Will publish service %p", &service);
#if 0
  if (_state != Collecting) { 
    Msg(Message::Warning, "FeeServer::Main::Publish", 
	"Wrong state (%d) to publish stuff in (not %d)", 
	_state, Collecting);
    return -Header::WrongState;
  }
#endif
  return _mon_thread.Publish(service);
}

//____________________________________________________________________
int
FeeServer::Main::Unpublish(const std::string& name)
{
#if 0
  if (_state != Collecting) { 
    Msg(Message::Warning, "FeeServer::Main::Unpublish", 
	"Wrong state %d (not %d)", _state, Collecting);
    return -Header::WrongState;
  }
#endif
  return _mon_thread.Unpublish(name);
}

//____________________________________________________________________
void
FeeServer::Main::RemoveServices()
{
  _mon_thread.RemoveServices();
}
    
//====================================================================
void
FeeServer::Main::ExitHandler(int code)
{
  const char* c = DimServer::getClientName();
  std::string client = (c ? c : "");
  if (!client.empty()) {
    Msg(Message::Warning, "Main", 
	"Ambitious user (%s) tried to kill fee server (%d), "
	"ignored the request", client.c_str(), code);
    return;
  }
  // Not a client request, so we die 
  Stop(DimExit);
}

//____________________________________________________________________
void
FeeServer::Main::ErrorHandler(int severity, int, char *msg)
{
  Message::Event event = Message::Info;
  switch (severity) {
  case 0:  event = Message::Info;    break;
  case 1:  event = Message::Warning; break;
  case 2:  event = Message::Warning; break; // Do not exit on DIM-error
  case 3:  event = Message::Error;   break;
  default: event = Message::Warning; break;
  }
  Msg(event, "DIM", msg);
}

//____________________________________________________________________
//
// EOF
//
