#include <feeserverxx/fee_cmd.hh>
#include <feeserverxx/fee_main.hh>
#include <feeserverxx/fee_packet.hh>
#include <feeserverxx/fee_dbg.hh>
#include <dim/dis.hxx>
#include <sstream>
#include <iostream>
#include <iomanip>

namespace FeeServer
{
  /** Handler of commands */
  class CommandHandler : public DimCommandHandler
  {
  public:
    /** Constructor 
	@param channel Reference to command channel
	@param server  Server name */
    CommandHandler(CommandChannel& channel, const std::string server) 
      : _command_channel(channel), 
	_command(0)
    {
      std::stringstream s;
      s << server << "_Command";
      char format[2] = { 'C', '\0' };
      _command = new DimCommand(s.str().c_str(), format, this);
      DimCommandHandler::itsCommand = _command;
    }
    /** Destructor */
    virtual ~CommandHandler() { if (_command) delete _command; }
  private:
    /** Not implemented */ 
    CommandHandler(const CommandHandler&);
    /** Not implemented */ 
    CommandHandler& operator=(const CommandHandler&);
    /** Handle the commands. In this class we simply turn the passed
	data into a proper format, and pass that on to the main
	object.  This object will in turn do the actual processing,
	possibly sending the request on to the control engine. */
    void commandHandler()
    {
      size_t          size = _command->getSize();
      const byte_ptr  data =  reinterpret_cast<const byte_ptr>(_command
							       ->getData());
      _command_channel.Handle(data,size);
    }
    /** Reference to command channel */
    CommandChannel& _command_channel;
    /** The command */
    DimCommand* _command;
  };
}
      

      
//____________________________________________________________________
FeeServer::CommandChannel::CommandChannel(const std::string& server, 
					  Main& main) 
  : _main(main),
    _cmd(),
    _handler(0)
{
  _handler = new CommandHandler(*this, server);
}

//____________________________________________________________________
FeeServer::CommandChannel::~CommandChannel()
{
  if (_handler) delete _handler;
}

//____________________________________________________________________
void
FeeServer::CommandChannel::Handle(const byte_ptr data, size_t size)
{
  _cmd._header_ok = _cmd.FromBuffer(data, size) != 0;
  // std::cout << "Got command:\n" << _cmd << std::endl;
    
  _main.HandleCommand(_cmd);
  DEXEC(5, 
	std::cout << "Got command:\n"
	<< _cmd << std::endl;);
  // free(data);
}

//____________________________________________________________________
//
// EOF
//
