#include <fee_dbg.hh>
#ifdef __DEBUG
# include <iostream>
# include <sstream>
# include <iomanip>
# include <cstdarg>
# include <cstdio>

int          FeeServer::DebugGuard::_level = 0;
unsigned int FeeServer::DebugGuard::_debug = 0;

//____________________________________________________________________
FeeServer::DebugGuard::DebugGuard(unsigned int lvl, 
				  const char* func, 
				  const char* file,
				  unsigned int line,
				  const char* fmt, ...) 
  : _message()
{
  if (lvl > _debug) return;
  static char buf[1024];
  va_list ap;
  va_start(ap,fmt);
#  ifdef WIN32
  vsprintf(buf, fmt, ap);
#  else
  vsnprintf(buf, 1024, fmt, ap);
#  endif
  va_end(ap);
  std::stringstream s;
  s << func << " [" << file << ":" << line << "] " << buf;
  _message = s.str();
  std::cout << std::setw(_level+2) << "=>" << _message << std::endl;
  _level++;
}

//____________________________________________________________________
FeeServer::DebugGuard::~DebugGuard()
{
  if (_message.empty()) return;
  _level--;
  std::cout << std::setw(_level+2) << "<=" << _message << std::endl;
}

//____________________________________________________________________
void
FeeServer::DebugGuard::Message(unsigned int lvl, 
			       const char*  func, 
			       const char*  file, 
			       unsigned int line, 
			       const char*  fmt, ...) 
{
  if (lvl > _debug) return;
  static char buf[1024];
  va_list ap;
  va_start(ap,fmt);
#  ifdef _WIN32
  vsprintf(buf, fmt, ap);
#  else
  vsnprintf(buf, 1024, fmt, ap);
#  endif
  va_end(ap);
  std::cout << std::setw(_level+2) << "==" 
	    << func << " [" << file << ":" << line << "] " << buf << std::endl;
}
#endif
//
// EOF
//
