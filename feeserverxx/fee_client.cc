#include <feeserverxx/fee_client.hh>
#include <feeserverxx/fee_sum.hh>
#include <dim/dic.hxx>
#include <sstream>
#include <iostream>
#include <climits>

//____________________________________________________________________
namespace FeeServer {
  struct InfoHandler : public DimInfoHandler
  {
  public:
    /** Constructor 
	@param Reference to the client */
    InfoHandler(Client& client) 
      : _client(client), 
	_msg_channel(0),
	_ack_channel(0)
    {
      std::stringstream msg_name;
      msg_name << _client._server << "_Message";
      _msg_channel = new DimInfo(msg_name.str().c_str(), 0, 0, this);
  
      std::stringstream ack_name;
      ack_name << _client._server << "_Acknowledge";
      _ack_channel = new DimInfo(ack_name.str().c_str(), 0, 0, this);
    }
    virtual ~InfoHandler()
    {
      if (_msg_channel) delete _msg_channel;
      if (_ack_channel) delete _ack_channel;
    }
  private:
    /** Not implemented */ 
    InfoHandler(const InfoHandler&);
    /** Not implemented */ 
    InfoHandler& operator=(const InfoHandler&);
    
    /** Handle updates */
    void infoHandler()
    {
      // Get the sender. 
      DimInfo* c = getInfo();
      
      // If there's no sender, return
      if (!c) return;
      size_t          size = c->getSize();
      unsigned char*  data = reinterpret_cast<unsigned char*>(c->getData());
      
      // Check which service was updated 
      if (c == _msg_channel) {
	if (_client._msg.FromBuffer(data, size) == 0) 
	  _client.NolinkMessage();
	else
	  _client.HandleMessage(_client._msg);
      }
      else if (c == _ack_channel) {
	if (_client._ack.FromBuffer(data, size)==0) 
	  _client.NolinkAcknowledge();
	else      
	  _client.DoHandleAcknowledge();
      }
      // if (size) free(data);
  
      // We got some unknown service!
      if (c && c != _msg_channel && c != _ack_channel) 
	_client.UnknownService(c->getName());
    }
    /** Reference to the client */ 
    Client& _client;
    /** Message channel */ 
    DimInfo* _msg_channel;
    /** Acknowledge channel */
    DimInfo* _ack_channel;
  };
}

//____________________________________________________________________
FeeServer::Client::Client(const std::string& server, 
			  const std::string& dns, 
			  unsigned int tries, 
			  unsigned int timeout) 
  : _handler(0),
    _server(server), 
    _cmd_name(),
    _msg(),
    _ack(),
    _cmd(),
    _buffer(0),
    _size(0),
    _tries(tries), 
    _timeout(timeout), 
    _reply(false), 
    _id(0)
{
  DimClient::setDnsNode(const_cast<char*>(dns.c_str()));
  _handler = new InfoHandler(*this);

  std::stringstream cmd_nme;
  cmd_nme << server << "_Command";
  _cmd_name = cmd_nme.str();
}

//____________________________________________________________________
FeeServer::Client::~Client()
{
  if (_handler) delete _handler;
}  

//____________________________________________________________________
bool
FeeServer::Client::SendCommand(unsigned short flags, 
			       std::vector<unsigned char>& data)
{
  _cmd._header._flags      = flags; 
  _cmd._header._error_code = 0;
  _cmd._header._id         = MakeId();
  _cmd._size               = data.size();
  _cmd._data               = data;
  _cmd._header.SetChecksum(&(_cmd._data[0]), _cmd._size);
  if (!_cmd.ToBuffer(_buffer)) {
    std::cerr << "Commmand error: failed to encode command" << std::endl;
    return false;
  }
  
  size_t osize = _cmd._size + sizeof(_cmd._header);
  //std::cout << "Command to send:\n" << _cmd << std::endl;
  _id    = _cmd._header._id;
  _reply = false;
  if (!DimClient::sendCommand(const_cast<char*>(_cmd_name.c_str()), 
			      &(_buffer[0]), osize)) {
    std::cerr << "Command error: failed to send" << std::endl;
    return false;
  }
  
  unsigned int sleep_time = (_timeout / _tries);
  size_t i = 0;
  for (; i < _tries; i++) {
    if (_reply) {
      data.resize(_size);
      if (_size > 0) std::copy(&(_buffer[0]), &(_buffer[_size]), &(data[0]));
    }
    break;
    // Sleep for some time. 
    usleep(sleep_time * 1000);
  }
  if (i >= _tries) { 
    Timeout();
    return false;
  }
  return true;
}


//____________________________________________________________________
unsigned int
FeeServer::Client::MakeId() 
{
  static unsigned int id = 0;
  id = (id + 1) % UINT_MAX;
  return id;
}

//____________________________________________________________________
void
FeeServer::Client::DoHandleAcknowledge() 
{
  // std::cout << "Got acknowledge:\n" << _ack << std::endl;
  if (_ack._header._id != _id) {
    UnexpectedReply(_ack._header._id, _cmd._header._id);
    return;
  }
  if (_ack._header._error_code < 0) { 
    _reply = true;
    _size  = 0;
    AcknowledgeError(_ack._header._error_code);
    return;
  }
  if (!_ack.ValidateChecksum()) {
    _reply = true;
    _size  = 0;
    ChecksumFailed(_ack._header._check_sum, 
		   FeeServer::CheckSum(&(_ack._data[0]), _ack._size));
    return;
  }
  // Copy data to buffer 
  if (_buffer.size() < _ack._size) _buffer.resize(_ack._size);
  std::copy(&(_ack._data[0]), &(_ack._data[_ack._size]), &(_buffer[0]));
  _size = _ack._size;
  _reply = true;
}

//____________________________________________________________________
//
// EOF
//
