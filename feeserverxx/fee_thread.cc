#include <feeserverxx/fee_thread.hh>
#include <feeserverxx/fee_lock.hh>
#include <feeserverxx/fee_main.hh>
#include <cerrno>
#include <csignal>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <cstring>

//____________________________________________________________________
namespace 
{
  FeeServer::Mutex _exec_mutex;
}

//====================================================================
void FeeServer::fee_thread_cleanup(void* arg)
{
  FeeServer::Thread* thread = static_cast<FeeServer::Thread*>(arg);
  if (!thread) { 
    std::cout << "No thread in clean-up " << std::endl;
    return;
  }
  thread->HandleCleanup();
}
 
//____________________________________________________________________
void* FeeServer::fee_thread_launcher(void* arg) throw ()
{
  FeeServer::Thread* thread = static_cast<FeeServer::Thread*>(arg);
  if (!thread) return 0;

  void* ret = 0;
  pthread_cleanup_push(fee_thread_cleanup, reinterpret_cast<void*>(thread));
  try {
    ThreadManager::Instance().Register(thread->_name, *thread);
    thread->_is_running = true;
    ret = thread->operator()();
    thread->_is_running = false;
  }
  catch (std::exception& e) {
    Main& m = Main::Instance();
    m.Msg(Message::Error, "Thread", "Caught exception: %s", e.what());
    thread->_is_running = false;
    m.HandleSignal(SIGINT);
  }
  pthread_cleanup_pop(1);
  return ret;
}

//====================================================================
FeeServer::Thread::Attributes::Attributes(bool detached, bool inherit, 
					  Scheduling schedule,  int priority)
  : _attr()
{
  pthread_attr_init(&_attr);
  Detach(detached);
  Inherit(inherit);
  Schedule(schedule, priority);
}

//____________________________________________________________________
FeeServer::Thread::Attributes::~Attributes()
{
  pthread_attr_destroy(&_attr);
}

//____________________________________________________________________
bool
FeeServer::Thread::Attributes::Detach(bool detached) 
{
  return pthread_attr_setdetachstate(&_attr,
				     (detached ?
				      PTHREAD_CREATE_DETACHED :
				      PTHREAD_CREATE_JOINABLE)) == 0;
}
//____________________________________________________________________
bool
FeeServer::Thread::Attributes::Inherit(bool inherit)
{
  return 
    pthread_attr_setinheritsched(&_attr,
				 (inherit ?
				  PTHREAD_INHERIT_SCHED : 
				  PTHREAD_EXPLICIT_SCHED)) == 0;
}
//____________________________________________________________________
bool
FeeServer::Thread::Attributes::Schedule(Scheduling policy, int priority)
{
  if (Inherits()) return true;
  bool ret = true;
  ret = pthread_attr_setschedpolicy(&_attr, 
				    (policy == Fifo ? SCHED_FIFO : 
				     (policy == RoundRobin ? SCHED_RR :
				      SCHED_OTHER))) == 0;
  if (ret && (policy == Fifo || policy == RoundRobin)) {
    struct sched_param p;
    p.sched_priority = priority;
    ret = pthread_attr_setschedparam(&_attr, &p) == 0;
  }
  return ret;
}

//____________________________________________________________________
bool
FeeServer::Thread::Attributes::Detached() const
{
  int state;
  pthread_attr_getdetachstate(&_attr, &state);
  return (state == PTHREAD_CREATE_DETACHED);
}
//____________________________________________________________________
bool
FeeServer::Thread::Attributes::Inherits() const
{
  int state;
  pthread_attr_getinheritsched(&_attr, &state);
  return (state == PTHREAD_INHERIT_SCHED);
}


//____________________________________________________________________
FeeServer::Thread::Attributes::Scheduling
FeeServer::Thread::Attributes::Policy() const
{
  int state;
  pthread_attr_getschedpolicy(&_attr, &state);
  return (state ==  SCHED_FIFO ? Fifo : 
	  (state == SCHED_RR   ? RoundRobin : Other));
}
  
//____________________________________________________________________
int 
FeeServer::Thread::Attributes::Priority() const
{
  struct sched_param state;
  pthread_attr_getschedparam(&_attr,&state);
  return state.sched_priority;
}

//====================================================================
FeeServer::Thread::Thread(const std::string& name)
  : _name(name), 
    _thread(),
    _is_running(false)
{
  if (!_name.empty()) return;
  std::stringstream s;
  s << this;
  _name = s.str();
}

//____________________________________________________________________
FeeServer::Thread::~Thread()
{
  if (IsCurrent()) Stop(0);
  else             Cancel();
}

//____________________________________________________________________
void
FeeServer::Thread::HandleCleanup()
{
  _is_running = false;
  ThreadManager::Instance().Deregister(_name);
  // std::cout << "Thread " << _name << " is exiting" << std::endl;
}

//____________________________________________________________________
FeeServer::Thread::Return
FeeServer::Thread::HandleReturn(int ret)
{
  if (ret != 0)
    std::cerr << "Error in thread: " << strerror(ret) << std::endl;
  switch (ret) { 
  case EAGAIN:  return Missing;
  case ESRCH:   return BadThread;
  case EINVAL:  return BadValue;
  case EDEADLK: return DeadLock;
  }
  return Success;
}

//____________________________________________________________________
const char*
FeeServer::Thread::Return2String(const Return& r)
{
  switch (r) {
  case Success:		return "Success"; 
  case Missing:		return "Missing system resources";
  case BadThread:	return "No such thread"; 
  case BadValue:	return "Operation not meaningful for thread"; 
  case DeadLock:	return "Would dead-lock";
  case Running:		return "Thread is already running"; 
  case NotCurrent:	return "Calling thread isn't this thread";
  case NotRunning:	return "Calling thread isn't running";
  }
  return "";
}

//____________________________________________________________________
unsigned int 
FeeServer::Thread::MaxThreads() 
{
#ifdef PTHREAD_THREADS_MAX
  return PTHREAD_THREADS_MAX;
#else 
  return 0xFFFFFFFF;
#endif
}

//____________________________________________________________________
bool
FeeServer::Thread::IsCurrent() const
{
  if (!_is_running) return false;
  return pthread_equal(pthread_self(), _thread) != 0;
}

//____________________________________________________________________
bool
FeeServer::Thread::operator==(const Thread& other) const
{
  return pthread_equal(other._thread, _thread) != 0;
}

    

//____________________________________________________________________
FeeServer::Thread::Return
FeeServer::Thread::Run() 
{
  Attributes attr;
  return Run(attr);
}

//____________________________________________________________________
FeeServer::Thread::Return
FeeServer::Thread::Run(const Attributes& attr) 
{
  // Guard g(_exec_mutex);
  if (_is_running) return Running;
  int ret = pthread_create(&_thread, &(attr._attr),
			   &fee_thread_launcher, (void*)this);
  return HandleReturn(ret);
}

//____________________________________________________________________
void
FeeServer::Thread::Stop(void* ret)
{
#ifndef ARM_TARGET
  if (!IsCurrent()) return; 
#endif
  ThreadManager::Instance().Deregister(_name);
  pthread_exit(ret);
}

//____________________________________________________________________
FeeServer::Thread::Return
FeeServer::Thread::Detach()
{
  if (!_is_running) return NotCurrent;
  return HandleReturn(pthread_detach(_thread));
}
  
//____________________________________________________________________
FeeServer::Thread::Return
FeeServer::Thread::EnableCancel(bool cancable) 
{ 
  // We only allow calls to this when we're in our own thread 
#ifndef ARM_TARGET
  if (pthread_equal(pthread_self(), _thread) == 0) return NotCurrent;
#endif
  if (!_is_running) return NotRunning;
  // if (!IsCurrent()) return NotCurrent;
  int old;
  return HandleReturn(pthread_setcancelstate((cancable ? 
					      PTHREAD_CANCEL_ENABLE : 
					      PTHREAD_CANCEL_DISABLE), &old));
}

//____________________________________________________________________
FeeServer::Thread::Return
FeeServer::Thread::AsyncCancel(bool async) 
{ 
  // We only allow calls to this when we're in our own thread 
#ifndef ARM_TARGET
  if (pthread_equal(pthread_self(), _thread) == 0) return NotCurrent;
#endif
  if (!_is_running) return NotRunning;
  // if (!IsCurrent()) return NotCurrent;
  int old;
  return HandleReturn(pthread_setcanceltype((async ? 
					     PTHREAD_CANCEL_ASYNCHRONOUS : 
					     PTHREAD_CANCEL_DEFERRED), &old));
}

//____________________________________________________________________
void
FeeServer::Thread::TestCancel()
{
#ifndef ARM_TARGET
  if (!IsCurrent()) return;
#endif
  pthread_testcancel();
}

//____________________________________________________________________
FeeServer::Thread::Return
FeeServer::Thread::Cancel()
{
  ThreadManager::Instance().Deregister(_name);
  return HandleReturn(pthread_cancel(_thread));
}

//____________________________________________________________________
FeeServer::Thread::Return
FeeServer::Thread::Join(void*& ret)
{
#ifndef ARM_TARGET
  if (IsCurrent()) return NotCurrent;
#endif
  int status = 0;
  if ((status = pthread_join(_thread, &ret)) == 0) 
    _is_running = false;
  return HandleReturn(status);
}

//____________________________________________________________________
FeeServer::Thread::Return
FeeServer::Thread::Join()
{
  void* ret = NULL;
  return Join(ret);
}

//====================================================================
struct MainThread : public FeeServer::Thread 
{
  MainThread() : FeeServer::Thread("main") 
  {
    _thread = pthread_self();
    _is_running = true;
  }
  void* operator()() { return 0; }
};

//====================================================================
FeeServer::ThreadManager* FeeServer::ThreadManager::_instance = 
new FeeServer::ThreadManager();

//____________________________________________________________________
FeeServer::ThreadManager::ThreadManager() 
  : _map(),
    _mutex("manager_mutex")
{
  _map["main"] = new MainThread;
}

//____________________________________________________________________
bool
FeeServer::ThreadManager::Register(const std::string& name, 
				   Thread& thread) 
{
  _mutex.Lock();
  ThreadMap::iterator i = _map.find(name);
  if (i != _map.end()) {
    _mutex.Unlock();
    return false;
  }
  // std::cout << "register thread " << name << std::endl;
  _map[name] = &thread;
  _mutex.Unlock();
  return true;
}
  
//____________________________________________________________________
void
FeeServer::ThreadManager::Deregister(const std::string& name)
{
  Guard g(_mutex);

  // Find the thread
  ThreadMap::iterator i = _map.find(name);
  if (i == _map.end()) return;

  // Will not deregister a running thread. 
  if (i->second->_is_running || 
      *(i->second) == *(_map["main"])) return;

  // Remove from map
  _map.erase(i);
}

//____________________________________________________________________
void
FeeServer::ThreadManager::Deregister(Thread& thread)
{
  Guard g(_mutex);
  if (thread._is_running) return;
  if (thread == *(_map["main"])) return;

  // Find the thread
  for (ThreadMap::iterator i = _map.begin(); i != _map.end(); ++i) {
    if (!i->second) continue; 
    if (thread == *(i->second) && !i->second->_is_running) {
      _map.erase(i);
    }
  }
}

//____________________________________________________________________
const std::string& 
FeeServer::ThreadManager::Current() const
{
  static std::string def("not found");
  // _mutex.Lock();
  // Find the thread
  for (ThreadMap::const_iterator i = _map.begin(); i != _map.end(); ++i) {
    if (!i->second) continue; 
    if (i->second->IsCurrent()) return (i->first);
  }
  // _mutex.Unlock();
  return def;
}
  
//____________________________________________________________________
void
FeeServer::ThreadManager::List() const 
{
  size_t max = 0;
  for (ThreadMap::const_iterator i = _map.begin(); i != _map.end(); ++i) {
    if (!i->second) continue; 
    max = std::max(max, i->first.size());
  }

  std::cout << "Threads registered:" << std::endl;
  for (ThreadMap::const_iterator i = _map.begin(); i != _map.end(); ++i) {
    if (!i->second) continue; 
    std::cout << "\t" << std::setw(max) << (i->first) 
	      << std::setw(8) << (i->second->_is_running ? "running" : "idle")
	      << " " << i->second->_thread << std::endl;
  }
}

//____________________________________________________________________
//
// EOF
//
