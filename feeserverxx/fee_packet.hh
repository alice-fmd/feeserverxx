#ifndef FEE_PACKET_HH
#define FEE_PACKET_HH
#include <feeserverxx/fee_bytes.hh>
#include <iosfwd>

namespace FeeServer 
{
  /** @defgroup packets Packets. 
      This group defines the structure of the communication channels
  */ 
  //__________________________________________________________________
  /** @class Message fee_packet.hh <feeserverxx/fee_packet.hh>
      @brief Message structure 
      @ingroup packets
  */
  struct Message 
  {
    /** Event types */
    enum Event { 
      /** Information */
      Info         = 1, 
      /** Warning */
      Warning      = 2, 
      /** Error */
      Error        = 4, 
      /** Failure */
      AuditFailure = 8, 
      /** Success */
      AuditSuccess = 16, 
      /** Debug */
      Debug        = 32, 
      /** Alarm */
      Alarm        = 64
    };
      
    /** Sizes of fields */
    enum {
      /** Size of detector field */
      DetectorSize    = 4,
      /** Size of source field */
      SourceSize      = 256,
      /** Size of description field */
      DescriptionSize = 256, 
      /** Size of date field */
      DateSize        = 20
    };
    /** Constructor */
    Message();
    /** Destructor */
    virtual ~Message() {}
    /** Initialize from a buffer */ 
    bool FromBuffer(unsigned char* buf, size_t size);
    /** Initialize from a buffer */ 
    bool ToBuffer(std::vector<unsigned char>& buf);
    /** Clear() */
    void Clear();
    /** Size of a message */ 
    static size_t Size();
    /** Convert the event code into a string */
    static const char* Code2String(int code);

    /** Event code */
    int  _event_type;
    /** Detector */
    char _detector[DetectorSize];
    /** Source */
    char _source[SourceSize];
    /** Description */
    char _description[DescriptionSize];
    /** Date stamp */
    char _date[DateSize];
  };
  //_________________________________________________________________
  /** Output streamer of a message 
      @ingroup packets
      @param o Output stream 
      @param m message
      @return reference to @a o */
  std::ostream& operator<<(std::ostream& o, const Message& m);

  //__________________________________________________________________
  /** @class Header fee_packet.hh <feeserverxx/fee_packet.hh>
      @brief Header of a fee packet (request or acknowledge) 
      @ingroup packets
  */
  struct Header
  {
    /** Flags */
    enum { 
      HasHuffman  = 0x0001, 
      HasChecksum = 0x0002
    };
    /** Error codes */
    enum { 
      /**  Execution was successful, no error */
      Ok = 0,
      /**  A timeout occured */
      TimeOut = 1, 
      /**  Nullpointer exception encountered */
      NullPointer = 2, 
      /** One of incoming parameters is invalid */
      InvalidParam = 3, 
      /** Operation was called in wrong state */
      WrongState = 4, 
      /** Execution simply failed */
      Failed = 5,
      /**  Initialisation of CE failed */
      InitFailed = 6, 
      /**  Command data corrupted, wrong checksum. */
      ChecksumFailed = 7,
      /**  Monitoring thread initialising failed. */
      MonitorFailed = 8,
      /**  Define for insufficient memory -> error */
      OutOfMemory = 9,
      /** error concering Threads (-initialization) */
      ThreadError = 10, 
      /** return val for published item name already exists */
      ItemExists = 11, 
      /** Start of detector specific error codes */ 
      DetectorBase = 20,
      /** Intercom Layer specific error codes */
      IclBase = 60,
      /** Unexpected return value encountered */
      Unknown = 99, 
      /** Maximum return value */
      MaxRetval = 99
    };
    /** Constructor 
	@param id    Acknowledge Id.  Copy of the request number. 
	@param error Return code 
	@param flags Flags  */
    Header(unsigned int id=0, short error=0, unsigned short flags=0);
    /** copy constructor */
    Header(const Header& other);
    /** Assignment operator */
    Header& operator=(const Header& other);

    /** Check that the check sum stored corresponds to the check sum
	of the passed buffer. 
	@param buf Buffer to check 
	@param size Size if buffer 
	@return true if the check sum matches, false otherwise */
    bool ValidateChecksum(const byte_ptr buf, size_t size) const;
    /** Calculate the check sum of passed buffer, and set the stored
	check sum. 
	@param buf Buffer calculate check sum for
	@param size Size if buffer */
    void SetChecksum(const byte_ptr buf, size_t size);

    /** Copy to a byte buffer 
	@param buffer Buffer to copy to. 
	@return number of bytes written to buffer */
    size_t ToBuffer(byte_array& buffer) const;
    /** Copy from a byte buffer 
	@param buffer Buffer to copy from. 
	@return number of bytes read from buffer */
    size_t FromBuffer(const byte_array& buffer);
    /** Copy from a byte buffer 
	@param buffer Buffer to copy from. 
	@param size Size of buffer
	@return number of bytes read from buffer */
    size_t FromBuffer(const byte_ptr buffer, size_t size);

    /** Size of the header structure */
    static size_t Size();
    /** Convert the event code into a string */
    static const char* Code2String(short code);

    /** Identifier of request/acknowledge */
    unsigned int   _id;
    /** Return code */
    short          _error_code;
    /** Flags of request/acknowledge */
    unsigned short _flags;
    /** Check sum of payload data */
    unsigned int   _check_sum;
  };
  //_________________________________________________________________
  /** Output streamer of a header 
      @ingroup packets
      @param o Output stream 
      @param m Header 
      @return reference to @a o */
  std::ostream& operator<<(std::ostream& o, const Header& m);
  
  //__________________________________________________________________
  /** @class Packet fee_packet.hh <feeserverxx/fee_packet.hh>
      @brief A packet.  Base class for other packet strutures.  It
      defines member functions to encode/decode a package to/from a
      byte buffer. 
      @ingroup packets */ 
  struct Packet 
  {
    /** Constructor 
	@param id    Acknowledge Id.  Copy of the request number. 
	@param error Return code 
	@param flags Flags 
	@param data  The return data block. 
	@param size  Size of the return block. */
    Packet(unsigned int id=0, short error=0, 
	   unsigned short flags=0, byte_ptr data=0, size_t size=0);
    /** Constructor 
	@param h The header to use 
	@param data The payload 
	@param size The size */ 
    Packet(const Header& h, byte_ptr data=0, size_t size=0);
    /** Desctructor */
    virtual ~Packet() {}
    
    /** Copy to a byte buffer 
	@param buf Bufffer to copy to.  The member function
	automatically enlarges the buffer if needed. 
	@return number of bytes written to buffer */
    size_t ToBuffer(byte_array& buf) const;
    /** Copy from a byte buffer 
	@param buf Buffer to copy from. 
	@param size Size of buffer 
	@return number of bytes copies */
    size_t FromBuffer(const byte_ptr buf, size_t size);
    /** Validate the check sum
    	@return true if the check sum matches, false otherwise */
    bool ValidateChecksum() const;

    /** Header */
    Header _header;
    /** Size of payloady */
    size_t         _size;
    /** Payload */
    byte_array _data;
  };
  //_________________________________________________________________
  /** Output streamer of a packet 
      @ingroup packets
      @param o Output stream 
      @param m packet
      @return reference to @a o */
  std::ostream& operator<<(std::ostream& o, const Packet& m);
    
  //__________________________________________________________________
  /** @class Acknowledge fee_packet.hh <feeserverxx/fee_packet.hh>
      @brief The acknowledge structure. 
      @ingroup packets
  */
  struct Acknowledge : public Packet
  {
    /** Constructor 
	@param id    Acknowledge Id.  Copy of the request number. 
	@param error Return code 
	@param flags Flags 
	@param data  The return data block. 
	@param size  Size of the return block. */
    Acknowledge(unsigned int id=0, short error=0, 
		unsigned short flags=0, byte_ptr data=0, size_t size=0) 
      : Packet(id, error, flags, data, size) 
    {}
    /** Constructor 
	@param h The header to use 
	@param data The payload 
	@param size The size */ 
    Acknowledge(const Header& h, byte_ptr data=0, size_t size=0) 
      : Packet(h, data, size) {}
    /** Destructor */ 
    virtual ~Acknowledge() {}
  };

  //__________________________________________________________________
  /** @class Command fee_packet.hh <feeserverxx/fee_packet.hh>
      @brief The command structure. 
      @ingroup packets
  */
  struct Command : public Packet
  {
    /** Server commands */ 
    enum { 
      /** Update server */
      UpdateServer = 0x4,
      /** Restart the server */ 
      RestartServer = 0x8, 
      /** Reboot */ 
      RebootMachine = 0x10, 
      /** Shutdown */
      ShutdownMachine = 0x20, 
      /** Exit server */ 
      ExitServer = 0x40, 
      /** Set Deadband 
	  Format of data is 
	  @verbatim 
	  data             ::= value services
	  value            ::= floating point number 
	  services         ::= service_name | service_wildcard 
	  service_name     ::= name of a service w/o server prefix 
	  service_wildcard ::= *_ string
	  @endverbatim
      */
      SetDeadband = 0x80,
      /** Get dead-band.  Data should contain service name. Returns
	  floating point number in acknowledge */
      GetDeadband = 0x100, 
      /** Set issue timeout. Data must contain a unsigned short. */
      SetIssueTimeout = 0x200, 
      /** Get issue timeout. Returns a unsigned short in acknowledge data */
      GetIssueTimeout = 0x400, 
      /** Set monitor update rate in milliseconds. Data must contain a
	  unsigned long. */ 
      SetUpdateRate = 0x800, 
      /** Get monitor update rate in milliseconds. Returns a unsigned
	  long in acknowledge data */ 
      GetUpdateRate = 0x1000, 
      /** Set log mask. Data must contain a unsigned int. */
      SetLogLevel = 0x2000, 
      /** Get log mask. Returns a unsigned int in acknowledge data */
      GetLogLevel = 0x4000, 
      /** Show monitored services */ 
      ShowServices = 0x8000,
      /** Suspend monitoring */ 
      SuspendMonitoring = 0x10000,
      /** Resume monitoring */ 
      ResumeMonitoring = 0x20000,
    };

    /** Constructor 
	@param id    Acknowledge Id.  Copy of the request number. 
	@param error Return code 
	@param flags Flags 
	@param data  The return data block. 
	@param size  Size of the return block. */
    Command(unsigned int id=0, short error=0, 
	    unsigned short flags=0, byte_ptr data=0, size_t size=0)
      : Packet(id, error, flags, data, size), _header_ok(false) 
    {}
    /** Destructor */
    virtual ~Command() {}

    /** Is the header OK? */
    bool _header_ok;
  };

}
#endif
//
// EOF
//
