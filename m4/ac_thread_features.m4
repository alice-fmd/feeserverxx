dnl -*- mode: autoconf -*-
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
AC_DEFUN([AC_THREAD_FEATURES],
[
  AC_LANG_PUSH([C++])
  dnl AC_REQUIRE([AC_THREAD_FLAGS])
  AC_CHECK_DECLS([PTHREAD_MUTEX_RECURSIVE, PTHREAD_MUTEX_RECURSIVE_NP],[],[],
	         [[#include <pthread.h>]])
  AC_CHECK_DECLS([PTHREAD_MUTEX_ERRORCHECK, PTHREAD_MUTEX_ERRORCHECK_NP],[],[],
	         [[#include <pthread.h>]])
  AC_LANG_POP([C++])
])
dnl
dnl EOF
dnl
