dnl -*- mode: autoconf -*-
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
dnl
dnl AC_ROOT_DIM([MINIMUM-VERSION 
dnl                   [,ACTION-IF_FOUND 
dnl                    [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_ROOT_DIM],
[
    AC_ARG_WITH([dim-prefix],
        [AC_HELP_STRING([--with-dim-prefix],
		[Prefix where Dim is installed])],
        dim_prefix=$withval, dim_prefix="")

    if test "x${DIM_CONFIG+set}" != xset ; then 
        if test "x$dim_prefix" != "x" ; then 
	    DIM_CONFIG=$dim_prefix/bin/dim-config
	fi
    fi   
    if test "x${DIM_CONFIG+set}" != xset ; then 
        if test "x$dim_prefix" != "x" ; then 
	    DIM_CONFIG=$dim_prefix/bin/dim-config
	fi
    fi   
    AC_PATH_PROG(DIM_CONFIG, dim-config, no)
    dim_min_version=ifelse([$1], ,0.1,$1)
    
    AC_MSG_CHECKING(for Dim version >= $dim_min_version)

    dim_found=no    
    if test "x$DIM_CONFIG" != "xno" ; then 
       DIM_CFLAGS=`$DIM_CONFIG --cflags`
       DIM_CPPFLAGS=`$DIM_CONFIG --cppflags`
       DIM_INCLUDEDIR=`$DIM_CONFIG --includedir`
       DIM_LIBS=`$DIM_CONFIG --libs`
       DIM_LIBDIR=`$DIM_CONFIG --libdir`
       DIM_LDFLAGS=`$DIM_CONFIG --ldflags`
       DIM_PREFIX=`$DIM_CONFIG --prefix`
       DIM_JNILIBS=`$DIM_CONFIG --libs`
       
       dim_version=`$DIM_CONFIG -V` 
       dim_vers=`echo $dim_version | \
         awk 'BEGIN { FS = " "; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       dim_regu=`echo $dim_min_version | \
         awk 'BEGIN { FS = " "; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $dim_vers -ge $dim_regu ; then 
            dim_found=yes
       fi
    fi
    AC_MSG_RESULT($dim_found - is $dim_version) 

    if test "x$dim_found" = "xyes" ; then 
       save_LDFLAGS=$LDFLAGS
       save_LIBS=$LIBS
       save_CPPFLAGS=$CPPFLAGS 

       LDFLAGS="$LDFLAGS $DIM_LDFLAGS"
       LIBS="$LIBS $DIM_LIBS"
       CPPFLAGS="$CPPFLAGS $DIM_CPPFLAGS" 

       # Change language 
       AC_LANG_PUSH(C++)

       have_dim_dim_hxx=0
       AC_CHECK_HEADERS([dim/dim.hxx],[have_dim_dim_hxx=1])
       
       # Check libraru 
       have_libdim=0
       AC_MSG_CHECKING([for -ldim])
       AC_LINK_IFELSE([AC_LANG_PROGRAM([#include <dim/dim.hxx>],
       				       [DimTimer timer])],
		      [have_libdim=1])
		     
       if test $have_dim_dim_hxx -gt 0 && test $have_libdim -gt 0 ; then 
           AC_DEFINE(HAVE_DIM)
       else
           dim_found=no
       fi

       # Restore 
       AC_LANG_POP(C++)
       CPPFLAGS=$save_CPPFLAGS
       LDFLAGS=$save_LDFLAGS
       LIBS=$save_LIBS
    fi	

    if test "x$dim_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(DIM_PREFIX)
    AC_SUBST(DIM_CFLAGS)
    AC_SUBST(DIM_CPPFLAGS)
    AC_SUBST(DIM_INCLUDEDIR)
    AC_SUBST(DIM_LDFLAGS)
    AC_SUBST(DIM_LIBDIR)
    AC_SUBST(DIM_LIBS)
    AC_SUBST(DIM_JNILIBS)
])

dnl
dnl EOF
dnl 
